# Upgrade guide

When upgrading one JOSF to another, you can simply follow this guide.

## From actionpack-1.2 to actionpack-1.3

Upgrading to 1.3 requires two actions;

- Download the upgrade package and extract the files.
- Replace the extracted files in `C:\Program Files\JOSF` to the correct directories

## From actionpack-1.1 to actionpack-1.2

Upgrading to 1.2 requires two actions;

- Download the upgrade package and extract the files.
- Replace the extracted files in `C:\Program Files\JOSF` to the correct directories

And that's it! Remember, if you need any help upgrading JOSF, [contact us!](../../about/contact.md)

## From actionpack-1.0 to actionpack-1.1

Upgrading to 1.1 requires two actions:

- Download the jar file and place it next to 1.0 in `C:\Program Files\JOSF` (you can leave the 1.0 if you want).
- Open the `start.ps1` file and edit this line

```powershell
	$josfVersion = "josf-actionpack-1.0";
```

- to this line

```powershell
	$josfVersion = "josf-actionpack-1.1";
```

And that's it!