# First run

This is the first-run example for JOSF 2.0. To see the **first-run** of JOSF 1.4 or below, [checkout this section](versions/1_4/first-run.md).

After installing JOSF 2.0, start JOSF by going to `C:\Program Files\JOSF\` and click the Start JOSF executable. This will open a command prompt (powershell) which will start JOSF. After a while, the command prompt tells us that JOSF is succesfully started by this line: 
```
  INFO: Succeeded in deploying verticle
```

Open you webbrowser and navigate to `localhost:8090/josf-app` and you will be greeted by our friendly JOSF.

![JOSF dashboard](img/josf-dashboard.png)

From here, click on the `Test cases` button, to create your first test case! [Head over to our tutorials section to **get things started!**](../tutorials/versions/current/index.md)