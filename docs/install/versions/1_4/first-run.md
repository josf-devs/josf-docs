#First run (JOSF 1.4 and below)
After installing JOSF, keep your PowerShell open and head over to `C:\Program Files\JOSF` by typing the following command:

````powershell
cd 'C:\Program Files\JOSF'
````

After that, you can start JOSF by executing the `start.ps1` command by typing the following command:

```powershell
.\start.ps1
```

JOSF is successfully started, when these messages are shown:

```powershell
Starting josf-actionpack-x.x
log4j:WARN No appenders could be found for logger (io.netty.util.internal.logging.InternalLoggerFactory).
log4j:WARN Please initialize the log4j system properly.
log4j:WARN See http://logging.apache.org/log4j/1.2/faq.html#noconfig for more info.
feb 07, 2018 9:10:05 AM io.vertx.core.Starter
INFO: Succeeded in deploying verticle
```

Any warnings displayed here are nothing to worry about. Just some notion that the logger can be configured.

**Remember**: always keep this PowerShell window open, as it serves as a console and JOSF will shutdown when you close it.

### Login to JOSF

When JOSF started successfully, open your favourite web browser, and navigate to http://localhost/josf. This will open the front-end of JOSF. To login, use the credentials as shown below.

#### Credentials

| Username | Password    |
| -------- | ----------- |
| Admin    | Admin123!!! |



### Turn JOSF off

After a day of testing and dancing, you'd want to shut down JOSF by going back to the PowerShell window and pressing `CTRL + c` key combination. This will gracefully shut down JOSF and all sub-processes that it started with. **Note**: It is possible that it will start a browser and load an empty webpage. Don't worry, this means that it also shutdown the Selenium Server nodes and hub.