# Installation guide

Installing JOSF single user package is as easy as running any other PowerShell script. When you’ve downloaded your copy of JOSF, follow these steps;

**NOTE: Make sure you have administrator privileges on your computer.**

1. Unzip the zipped JOSF package with your favorite unzipping tool.
2. Open a PowerShell window **as an administrator**
3. Navigate to where you have placed your unzipped JOSF copy
4. Enter `.\install.ps1` from the root of the JOSF installer directory
   ![Install JOSF PowerShell](img/install.png)
5. The installer will now copy all required files to the `C:/Program Files/JOSF` directory

After completing the installation, head over to the next page to run JOSF for the first time!