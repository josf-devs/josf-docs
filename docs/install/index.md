# Installation guide

Installing JOSF single user package is as easy as running double clicking on an icon!
**Note: Make sure you have administrator privileges on your computer.**

1. Unzip the zipped JOSF package with your favorite unzipping tool.
2. Navigate to where you have placed your unzipped JOSF copy
3. Open the `Install JOSF` executable.
4. When asked for installing JOSF at the default location, enter "Y"
5. Wait for JOSF to install, and enjoy!

After completing the installation, head over to the next page to run JOSF for the first time!

**Note**: If you'd like to install JOSF at a *different location*, or you'd like to install a *server variant* to share the same JOSF installation with your co-workers, you can [always contact us!](../about/contact.md)
