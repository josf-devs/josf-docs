# Frequently Asked Questions

We hope we can help you with your questions, with this FAQ page. If your question is not answered here, don't hesitate to [contact us](../about/contact.md)!

## Installing JOSF on my local machine

Having trouble with installing JOSF on your local machine? Let's see if we can help you with that.

### When installing JOSF, the message 'PowerShell Script Not Digitally Signed' comes up

As we are working for a solid solution, for now you can bypass this certificate error by executing the following command (via PowerShell):

```powershell
Set-ExecutionPolicy -ExecutionPolicy Bypass
```

This bypasses the certificate warning you are confronted with.

### When installing JOSF, my computer cannot load scripts because of the execution policy

Are you facing an error as shown here;

```powershell
.\install.ps1 : File [JOSF FILES LOCATION] cannot be loaded because running scripts is disabled on this system. F
or more information, see about_Execution_Policies at https:/go.microsoft.com/fwlink/?LinkID=135170.
At line:1 char:1
+ .\install.ps1
+ ~~~~~~~~~~~~~
    + CategoryInfo          : SecurityError: (:) [], PSSecurityException
    + FullyQualifiedErrorId : UnauthorizedAccess
```

Please execute this command, to tell Windows it's OK to install JOSF with this installation file.

```powershell
Set-ExecutionPolicy -ExecutionPolicy Bypass
```

## Starting JOSF on my local machine

You've successfully installed JOSF, but are having trouble starting JOSF.

### Java is not recognized as the name of a cmdlet

For now, we need you to install the correct version of Java to run JOSF. Any version of Java 8 (Java Runtime Environment or Java Development Kit 1.8) will do.

### ClassNotFoundException is shown when staring JOSF

You've probable installed Java, but not the correct version. Any version of Java 8 (Java Runtime Environment or Java Development Kit 1.8) will do.

## Run time errors

You've successfully started JOSF, but are facing run time errors.

### Browser not starting
First, check wheter the selenium server node is up and running by opening a browser and head over to; http://localhost:4444/grid/console. Verify it looks something like this.![Selenium grid console](./img/selenium-grid-console.png)

If it doesn't, this means JOSF had trouble starting the Selenium Hub. Make sure you are running JOSF with elevated rights (e.g. as administrator).

Next, check whether the individual nodes are up and running, and whether you can manually start a new session.

- For Chrome, visit http://localhost:7777/wd/hub/static/resource/hub.html
- For Internet Explorer, visit http://localhost:6666/wd/hub/static/resource/hub.html
- For Firefox, visit http://localhost:5555/wd/hub/static/resource/hub.html

Click the create `new session`-button and verify the corresponding browser is starting. If not;

- Make sure you have installed that browser on your local machine.
- If necessary, upgrade your browser drivers correspondingly.

Next, make sure your virus scanner did not container it in any way.

If non of these apply to your situation, don't hesitate to [contact us](../about/contact.md) and we'll help you ASAP!

## Using JOSF

You are happily using JOSF, but run into some questions that need to be answered.

### I can't find a command that was available in Selenium IDE.

Some commands which were available in Selenium IDE were already deprecated there. We took the liberty to not implement all these commands into JOSF. Below is a list of the Selenium IDE commands that JOSF does not support out of the box. However, you can always implement them yourself if you realy think you can't live without them.

#### Table: Deprecated by Selenium IDE, unsupported by JOSF*:

| Selenium IDE command | Alternative     | Some remarks from our side                                   |
| -------------------- | --------------- | ------------------------------------------------------------ |
| keyDown              | sendKeys        |                                                              |
| keyDownAndWait       | sendKeysAndWait |                                                              |
| keyPress             | sendKeys        | Not to be confused with the `keypress` action available in JOSF |
| keyPressAndWait      | sendKeysAndWait |                                                              |
| keyUp                | sendKeys        |                                                              |
| keyUpAndWait         | sendKeysAndWait |                                                              |

*This list is not complete yet.