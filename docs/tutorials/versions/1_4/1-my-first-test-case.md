# My first dance moves

Congratulations! You've successfully installed JOSF and now you want to enjoy it's dancing moves! Although it already knows al the steps, you must teach him how to dance.

For this tutorial, we'll be assuming you are running JOSF on your localhost (and are logged in).

## Automating the Google search websites

For this first lesson, we shall automate the Google search field at http://www.google.com/. Even better, the calculator behind this search engine.

### Develop a new dance

To create a new dance, or test case, go to http://localhost/josf/node/add/test_case and give your test case a catchy title. We'll go for: Calculate some numbers! For the base url, enter: http://www.google.com.

Each test case consists of a couple of dance moves, called test steps. A test step is simply an action, an object locator and sometimes some test data. For now, follow these steps;

* select `type` from the list of actions
* as Object locator, enter `id=lst-ib`
* for the Test data, enter `10 + 15 + 19 + 6`

Now we've entered a simple calculation to the search field of google. Next we need to press enter to do some actual calculations. Press the `add another item`, to add a new test step.

- select `submit` from the list of actions
- as Object locator, enter `id=lst-ib`
- leave the Test data field empty

After this, leave every thing as it is and press the `Save` button.

### Start dancing

After saving, your test case is saved into JOSF and you can execute the test by clicking the `Execute test` link in the JOSF tools (left of the screen).

If all is well, you've just made JOSF dance and your test was executed. After each test, you'll be brought to the testresult page. Everything is OK, or even better, each step results in an INFO state. This is because we haven't tested the result. Let's do that now.

### Some actual testing

A test is, in essence, a verification of an expected outcome, so let's continue to add a last verification step.

Navigate to Content -> Calculate some numbers! -> Edit, so we can add the last dance move.

- select `verifyText` from the list of actions
- as Object locator, enter `id=cwos`
- for the Test data, enter 50

Save your test case, run your test again and see that JOSF has validated the value 50 that passes! Of course you can try some other calculations yourself! Also try to verify a wrong number (like 49), so you can see what happens then.

### A closer look at a test step

You've probably finished the tutorial by now, and maybe you are wondering, what is it I'm doing? Well, let's get into that by explaining the basics of JOSF.

JOSF is defined as a keyword driven framework. It listens to keywords, or test actions in our case, that it will execute. A lot of these actions are preprogrammed for you, but you can actually extend these actions to fit your needs. Before we get there, first let's take a closer look at the design of a test step.

#### An action

The action is the keyword for JOSF to execute. This triggers some code to be executed like clicking on buttons, validating text, typing into fields, etc. Although alot of preprogrammed actions are available, if you are comfortable developing in Java, you can add your own actions! Actions sometimes requires an object locator, sometimes test data and sometimes both.

#### The test data

This is the actual data that you use to test with. This can be values to enter into an input field, or some expected value when verifying data.

#### An object locator

Probably the most techincal part of a test step. The technical location of an object so that JOSF knows on what object you'd like to perform your action. In the example in this tutorial, the object locator was `id=lst-ib` for the search field on the Google searchpage. This locator contains two elements:

1. the location strategy (`id`); and,
2. the (unique) location value (`lst-ib`).

The location strategy `id` allows us to make use of the id of the object. Other location strategies are; `className`, `cssSelector`, `linkText`, `name`, `partialLinkText`, `tagName`, and `xpath`. Next, is the location value. To know what location value to use.

For more information on locating object, continue to the next tutorial!