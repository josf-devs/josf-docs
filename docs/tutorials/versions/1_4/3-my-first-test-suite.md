# My first test suite 

Creating a lot of test cases becomes ideal if you could chain them into a logical order of execution. This is possible by creating a new test suite. To make use of the power of chaned test cases, we shall first add a new test case.

For this tutorial, we'll be asuming you are running JOSF on your localhost (and are logged in).

## Calculations with words

In the first tutorial, we've done some calculations with numbers. But the Google calculator is also capable of calculation based on written numbers like `seventy four times two`.

Create a **new test case** and verify that the result of the search terms `seventy four times two` actually is `148`. It may help to peek into the first tutorial on how that's done. After creating your test case (and made sure it passes), return to this tutorial.

## Create a new test suite

Creating a new test suite is much like creating a new test case. It's adding new content to JOSF like before; Content -> Add content -> Test suite. Or follow the link http://localhost/josf/node/add/test_suite to go there directly.

A test suite has a title, a description and one or more test cases. 

1. Enter `Calculate with Google` as a title.

2. Come up with some descriptive text like:

   ``````
   The Google Calculator is able to calculate with numbers and words.
   ``````

3. Enter the title of the first test case you've created. Ours was called `Calculate some numbers!`.

4. Press the `add another item` button.

5. Enter the title of the second test case you've created. Ours was called `Calculate with words!`.

6. Finally save your test suite.

### Execution of a test suite

You can review and execute from the following screen by clicking the `Execute suite` link in the JOSF tools box on the left.

As you've probably noticed, there is only one browser open for the duration of the test suite. This allows for advanced chaining of test cases in the same browser session.