# Importing from Selenium IDE

If you either have a bunch of pre-recorded Selenium IDE scripts, or you want to add a test case to JOSF very fast, importing your Selenium IDE scripts is the way to dance!

If you're not familiar with Selenium IDE, in short; it's a record and playback tool. You press a record button, perform your test in the web browser, and Selenium IDE records all your movements.

## A word of caution

Selenium IDE is a beautiful product. Unfortunately, as of Firefox 55, [Selenium IDE is deprecated](https://seleniumhq.wordpress.com/2017/08/09/firefox-55-and-selenium-ide/). Luckily, there is a new Selenium IDE (Chrome) extension on the rise, which hopefully become stable very soon!

Even though the Selenium IDE Firefox plugin and the new extension are very different in it's export modal (xml/html and json/side), JOSF is capable of importing both styles!

**Note** that this is not a Selenium IDE tutorial, and we're assuming that you already have worked with Selenium IDE before.

## Selenium IDE (deprecated version)

The result of this tutorial is to import a (pre) recorded Selenium IDE script into JOSF via a form. 

To get familiar with Selenium IDE and the import mechanism, we'll create a new test case in Selenium IDE, called Recorded Google calculations.

If you haven't already, open Selenium IDE in Firefox and record a simple test case in the Google search bar. For example, calculating some numbers!

- Record your test case
- Verify it works by pressing the play button

After recording, you should have something like this;

![selenium-ide-recorded](img/selenium-ide-recorded.png)

This source is actually an HTML styled script. Head over to the source tab and see for yourself!

![selenium-ide-source](img/selenium-ide-source.png)

Copy the entire HTML script and head over to http://localhost/josf/admin/config/test/josf/import. You need to provide a test case title yourself, so enter something like 'Recorded Google calculations' in the title field and paste the Selenium IDE source in the source text area.

![josf-import-screen](img/josf-import-screen.png)

Next, press Generate and let JOSF do the magic. After the import is complete, your test case title is shown in the next screen. 

![josf-import-completed-screen](img/josf-import-completed-screen.png)

Next head over the content page of JOSF to verify your imported test case is created!

![josf-imported-node](img/josf-imported-node.png)

You can now execute this test as any other test in JOSF.



## Selenium IDE (chrome extension version)

As mentioned above, a newer version of Selenium IDE is released and of course JOSF is capable of importing these scripts as well!

There is a small difference though. The deprecated version had a `source` tab, which allowed easy copy-paste of the source into JOSF. This version of Selenium IDE doesn't have that feature, so we are going to save the file (with a .side file-extension), open that file and copy-past the source into JOSF like that.

To get familiar with Selenium IDE and the import mechanism, we'll create a new test case in Selenium IDE, called Recorded Google calculations.

If you haven't already, open Selenium IDE in chrome and record a simple test case in the Google search bar. For example, calculating some numbers!

- Record your test case
- Rename your test case
- Rename your project (in the top)
- Verify it works by pressing the play button

After recording, you should have something like this;

![selenium-ide-chrome-recorded](img/selenium-ide-chrome-recorded.png)

Save your project on disk somewhere, name it 'Recorded Google calculations.side' and reopen it with your favourite text editor. 

Upon opening this file, you'll see a large json formatted test case. Copy all of this text and head over to http://localhost/josf/admin/config/test/josf/import. You **won't** need to provide a test case title yourself as it can extract it from the json file. 

Paste the json formatted source code into the text area and press generate.

![josf-import-side-screen](img/josf-import-side-screen.png)

A screen will appear, stating a title 'It got imported!' so we can head over to the content page of JOSF and review our imported test case!

![josf-import-side-completed-screen](img/josf-import-side-completed-screen.png)

And the content page;

![josf-imported-node](img/josf-imported-node.png)

### Adding suites

The advantage of having a structured json file, is that whenever you create a test suite in Selenium IDE, and add test cases to those suites, JOSF creates the required suites and inserts the required test cases. Isn't that neat! 