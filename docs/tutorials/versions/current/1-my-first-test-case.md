# My first dance moves

Congratulations! You've successfully installed JOSF and now you want to enjoy it's dancing moves! Although it already knows all the steps, you must teach him how to dance.

For this tutorial, we'll be assuming you are running JOSF on your localhost.

## Automating the Google search websites

For this first lesson, we shall automate the Google search field at http://www.google.com/. Even better, the calculator behind this search engine.

### Develop a new dance

To create a new dance, or test case, open the JOSF dashboard, and click the `Test cases` button. This leads you to the list of all your test cases. From here, click the `Create new test case` to create a new test case.

Give your test case a catchy title. We'll go for: Calculate some numbers! Next, open the settings and set-up the base-url. By default, this is **about:blank**. This should be the domain of your application, and in this tutorial we'll set it to **http://www.google.com**.

Save your settings by pressing the `Save` button and then save your test case by pressing the `Save` button (or use the shortcut `control + s`).

Each test case consists of a couple of dance moves, called test steps. A test step is simply an action, an object locator and sometimes some test data. For now, follow these steps;

Add a new test step by pressing the `Add Action` button.

* select `type` from the list of actions
* as Object locator, enter `name=q`
* for the Test data, enter `10 + 15 + 19 + 6`

Now we've entered a simple calculation to the search field of Google. Next we need to press enter to do some actual calculations. Press the `Add Action` button, to add a new test step.

- select `submit` from the list of actions
- as Object locator, enter `name=q`
- leave the Test data field empty

After this, leave every thing as it is and press the `Save` button.

### Start dancing

After saving, your test case is saved into JOSF and you can execute the test by clicking the `Execute` button. Sit back and enjoy how JOSF starts a browser, navigates to the *base-url* and executes your test steps.

If all is well, you've just made JOSF dance and your test was executed. 

### Some actual testing

A test is, in essence, a verification of an expected outcome, so let's continue to add a last verification step.

Navigate to Content -> Calculate some numbers! -> Edit, so we can add the last dance move.

- select `verifyText` from the list of actions
- as Object locator, enter `id=cwos`
- for the Test data, enter 50

Save your test case, run your test again and see that JOSF has validated the value 50 that passes! Of course you can try some other calculations yourself! Also try to verify a wrong number (like 49), so you can see what happens then.

### A closer look at a test step

You've finished the first part of the tutorial now, and maybe you are wondering, what is it I'm doing? Well, let's get into that by explaining the basics of JOSF.

JOSF is defined as a keyword driven framework. It listens to keywords, or test actions in our case, that it will execute. A lot of these actions are preprogrammed for you, but you can actually extend these actions to fit your needs. Before we get there, first let's take a closer look at the design of a test step.

#### An action

The action is the keyword for JOSF to execute. This triggers some code to be executed like clicking on buttons, validating text, typing into fields, etc. Although alot of preprogrammed actions are available, if you are comfortable developing in Java, you can add your own actions! Actions sometimes requires an object locator, sometimes test data and sometimes both.

#### The test data

This is the actual data that you use to test with. This can be values to enter into an input field, or some expected value when verifying data.

#### An object locator

Probably the most techincal part of a test step. The technical location of an object so that JOSF knows on what object you'd like to perform your action. In the example in this tutorial, the object locator was `name=q` for the search field on the Google searchpage. This locator contains two elements:

1. the location strategy (`name`); and,
2. the (unique) location value (`q`).

The location strategy `name` allows us to make use of the name of the object. Other location strategies are; `className`, `cssSelector`, `linkText`, `id`, `partialLinkText`, `tagName`, and `xpath`. Next, is the location value. To know what location value to use.

For more information on locating object, continue to the next tutorial!