# Data table: Mapping for lists

An example;
```
Given this flight data
  | KMSY | 29.993333 |  -90.258056 |
  | KSFO | 37.618889 | -122.375000 |
  | KSEA | 47.448889 | -122.309444 |
  | KJFK | 40.639722 |  -73.778889 |
 ```

In the step definition, we can extract the data with the `${table.row name.#}` syntax. If you aren't quite familiar with buffers, take a look at [buffers and the Set keyword](\actions\selenese\set.md).

For example, if we need the `-90.258056` data from the list, we can use `${table.KMSY.1}` or when we want to have `47.448889`, we use `${table.KSEA.0}`. First we select the row name, then we select the column index to extract the cell data.

| Action name | Object   | Data            | Result             |
|-------------|----------|-----------------|--------------------|
| type        | name=lat | ${table.KMSY.0} | Types '29.993333'  |
| type        | name=lon | ${table.KMSY.1} | Types '-90.258056' |
