# Data table: Multiple mappings

Multiple mappings is a table with column names. You can iterate over them using a `Repeating group`.

An example;
```
Given these people go to the party
  | first name | last name | occupation |
  | John       | Deere     | Farmer     |
  | Tim        | Cook      | Traveler   |
  | Eric       | Foreman   | Legend     |
```

In the step definition, we can extract the data with the `${table.#.column name}` syntax. If you aren't quite familiar with buffers, take a look at [buffers and the Set keyword](\actions\selenese\set.md).

For example, if we need the `John` data from the list, we can use `${table.0.first name}` or when we want to have `Foreman`, we use `${table.2.last name}`. First we select the row number, then we select the column name to extract the cell data.

Using this is very much like using a multiple lists, but with one great difference; you can use this one within a `Repeating group`!

When using the Repeating group, with the repeater set at `Data table`, you can iterate over your table. JOSF will take care of getting the correct data out of your list, as long as you provide the right syntax; **`${table.#.column name}`** (see, no index number, just the hashtag sign).

| Action name | Object          | Data                  | Result |
|-------------|-----------------|-----------------------|--------|
| type        | name=firstname  | ${table.0.first name} |        |
| type        | name=lastname   | ${table.0.last name}  |        |
| type        | name=occupation | ${table.0.occupation} |        |
| click       | name=addPerson  |                       |        |

JOSF will now iterate over your test steps within the Repeating group, as long as there is data. In this example, it will iterate three times!
