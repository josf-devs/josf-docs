# Data table: Mapping

A mapping holds a set of keys and values and can be used to store and extract named data.
An example;
```
Given I own a few animals
  | Cows     | 2  |
  | Chickens | 7  |
  | Dogs     | 1  |
  | Cats     | 12 |
```

In the step definition, we can extract the data with the `${table.key}` syntax. If you aren't quite familiar with buffers, take a look at [buffers and the Set keyword](\actions\selenese\set.md).

For example, if we need the `7` data from the list, we can use `${table.Chickens}`.
This is great for reusing the step definition.


| Action name | Object            | Data              | Result                                |
|-------------|-------------------|-------------------|---------------------------------------|
| verifyValue | name=noOfChickens | ${table.Chickens} | Verifies the field for the value of 2 |
