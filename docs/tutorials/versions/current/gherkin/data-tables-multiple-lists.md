# Data table: Multiple lists

Multiple lists holds a list of lists.

An example;
```
Given these people go to the party
 | John       | Deere     | Farmer     |
 | Tim        | Cook      | Traveler   |
 | Eric       | Foreman   | Legend     |
```

In the step definition, we can extract the data with the `${table.#.#}` syntax. If you aren't quite familiar with buffers, take a look at [buffers and the Set keyword](\actions\selenese\set.md).

For example, if we need the `John` data from the list, we can use `${table.0.0}` or when we want to have `Foreman`, we use `${table.2.1}`. First we select the row number, then we select the column number to extract the cell data.

This is great for reusing the step definition.

Here is an example of using that in your actions.

| Action name | Object         | Data         | Result        |
|-------------|----------------|--------------|---------------|
| type        | name=firstname | ${table.0.0} | Types 'John'  |
| type        | name=lastname  | ${table.0.1} | Types 'Deere' |
| click       | name=addPerson |              |               |
| type        | name=firstname | ${table.1.0} | Types 'Tim'   |
| type        | name=lastname  | ${table.1.1} | Types 'Cook'  |
| click       | name=addPerson |              |               |
