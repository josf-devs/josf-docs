# Data table: List

The list is the most simplistic data table. It represents a list of words, that you may use in your test data.
An example;
```
Given I have a shopping list
  | Apples  |
  | Pears   |
  | Oranges |
  | Milk    |
```

In the step definition, we can extract the data with the `${table.#}` syntax. If you aren't quite familiar with buffers, take a look at [buffers and the Set keyword](\actions\selenese\set.md).

For example, if we need the `Oranges` data from the list, we can use `${table.2}` as it's the item with the index of 2 (lists start at an index of `0`) 

| Action name | Object         | Data       | Result          |
|-------------|----------------|------------|-----------------|
| type        | name=searchbox | ${table.2} | Types `Oranges` |
| verifyValue | name=searchbox | Oranges    | Results to PASS |

But that's not all. The power of lists really come into play when you combine them with a `Repeating group`

When using the Repeating group, with the repeater set at `Data table`, you can iterate over your list. JOSF will take care of getting the correct data out of your list, as long as you provide the right syntax; **`${table.#}`** (see, no index number, just the hashtag sign).

| Action name | Object         | Data       | Result |
|-------------|----------------|------------|--------|
| type        | name=searchbox | ${table.#} |        |
| click       | name=find      |            |        |

JOSF will now iterate over your test steps within the Repeating group, as long as there is data. In this example, it will iterate four times!