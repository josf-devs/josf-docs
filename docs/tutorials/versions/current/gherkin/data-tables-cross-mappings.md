# Data table: Cross mapping

An example;
```
Given this flight data
  |      |       lat |         lon |
  | KMSY | 29.993333 |  -90.258056 |
  | KSFO | 37.618889 | -122.375000 |
  | KSEA | 47.448889 | -122.309444 |
  | KJFK | 40.639722 |  -73.778889 |
```

In the step definition, we can extract the data with the `${table.row name.column name}` syntax. If you aren't quite familiar with buffers, take a look at [buffers and the Set keyword](\actions\selenese\set.md).

For example, if we need the `-90.258056` data from the list, we can use `${table.KMSY.lon}` or when we want to have `47.448889`, we use `${table.KSEA.lat}`. First we select the row name, then we select the column name to extract the cell data.

| Action name | Object   | Data              | Result             |
|-------------|----------|-------------------|--------------------|
| type        | name=lat | ${table.KMSY.lat} | Types '29.993333'  |
| type        | name=lon | ${table.KMSY.lon} | Types '-90.258056' |
