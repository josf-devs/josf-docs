# Gherkin

That's right, JOSF understands your natural language! If you're not familiar with gherkin, the guys at at Cucumber.io [have it all written down] (https://cucumber.io/docs/guides/overview/).


An example of one of our own feature files.
```text
Feature: use Gherkin language to improve Behaviour Driven Development
  As we are a backer of BDD,
  JOSF enables us to develop feature files, like this one!

  Scenario: Reusing step definitions is extremely easy
    Given I'm at the grocery store
    When I buy milk
     And I buy flowers
     And I buy bread
    Then the total amount is $6.89

  Scenario: Data tables are also implemented
    Given I'm at the grocery store
    When I have these items on my list
      | milk    |
      | flowers |
      | bread   |
    Then the total amount is $6.89
```