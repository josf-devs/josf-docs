# Data tables

Data Tables are a unique feature within Gherkin and implemented within JOSF.

There are six types of data tables;

 - [List](./data-tables-list.md)
 - [Mapping](./data-tables-mapping.md)
 - [Multiple lists](./data-tables-multiple-lists.md)
 - [Multiple mappings](./data-tables-multiple-mappings.md)
 - [Mapping for lists](./data-tables-mapping-for-lists.md)
 - [Cross mappings](./data-tables-cross-mappings.md)

This may dazzle you, but don't worry, we'll take you through them, one by one.
Each type of data table has it's specific use case. It may be very well so that you'll use 
only one type of data table, and that's OK.