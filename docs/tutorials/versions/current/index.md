# Tutorials

Note that these tutorials are for the latest version of JOSF. Looking for an older version? Check the `Older versions` section from the menu.

Let's start at the very beginning. Open the first tutorial "My first dance moves"!