# Making some configurations

Did you known that you can make JOSF dance in all kinds of situations. As of now, JOSF can dance with any web browser you tell it to dance with!

## Setting different browsers

By default, JOSF is setup to execute its dance moves in Google's Chrome web browser. You can alter this to Microsoft's Internet Explorer or to Mozilla's Firefox. Note that you'll need to have these browsers installed on your local machine.

### Change settings

To change settings, open a test case, then open its settings, and change the Browser to your liking. 
Here you can set the browser to Firefox or Internet Explorer. If you have any of these browsers installed, go ahead and try them. Simple select the pre-configured *Remote Firefox*I or *Remote Internet Explorer* and press *Save*. The changes take effect immidiatly.

Now, execute your test again and see that it will start in your choosen browser!

## Browser configuration

The three browsers; *Remote Chrome*, *Remote Firefox* and *Remote Internet explorer* are pre-configured by our JOSF developers for you. You can add, remove and define your own configurations yourself. This is done outside of the web-interface of JOSF and in the configuration directory.

Open the configuration directory from the root JOSF directory by going to **src/main/conf**. There you'll see a couple of configuration files, but let's focus on *the driver-list.json* file.

As the file extension shows us, this is a JSON file. A JSON file is a structured data file, that in this case contains information about which browsers we can use. Each browser is defined in the `drivers` element, and has at least a `displayName`, `hubUri`, `browserName`, `platform` and `capabilities`. The `capabilities` element is a collection of extra capabilities, but we'll address that later.

```json
{
  "drivers": [
    {
      "displayName": "Remote Firefox",
      "hubUri": "http://localhost:4444/wd/hub",
      "browserName": "firefox",
      "platform": "WINDOWS",
      "capabilities": {}
    },
    {
      "displayName": "Remote Internet Explorer",
      "hubUri": "http://localhost:4444/wd/hub",
      "browserName": "internet explorer",
      "platform": "WINDOWS",
       "version": "11",
      "capabilities": {
        "ignoreProtectedModeSettings": true,
        "ignoreZoomSetting": true,
	    "ie.ensureCleanSession": true
      }
    },
    {
      "displayName": "Remote Chrome",
      "hubUri": "http://localhost:4444/wd/hub",
      "browserName": "chrome",
      "platform": "WINDOWS",
      "version": "58",
      "capabilities": {}
    }
  ]
}
```
What you see here, is the default JSON configuration. Let's break this down.

For each possible browser, a JSON object is present. For example, the Chrome browser is enabled by this block.

```json
{
  "displayName": "Remote Chrome",
  "hubUri": "http://localhost:4444/wd/hub",
  "browserName": "chrome",
  "platform": "WINDOWS",
  "version": "58",
  "capabilities": {}
}
```

Here, a `displayName`, `hubUri`, `browserName`, `platform`, `version` and `capabilities` are set for the Chrome browser. The `displayName` is the name that is displayed in the settings pop-up screen.

The rest is a bit more technical. The rest of the configuration is required to actually start the correct browser. As you review the entire *driver-list.json*, you can see that the `hubUri` is equal for all. This is because that is the `uri` of the Selenium Server is running at that address. To understand this all, you must understand the concept of Selenium Server.

### Selenium Server

As you may know, Selenium is a way to automate browsers. This is what is used in the core of JOSF, to execute actions against your web application. For JOSF, Selenium Server is nothing more than a resource pool of configured browsers that you can select to test against. The Selenium Server consists of one hub and one or more nodes. ![Selenium Server, hub and nodes](img/selenium-hub-nodes.png)

By default, during start-up, JOSF also starts a Selenium Hub and three pre-configured Selenium nodes, depicted here. It is possible to add as many nodes as you'd like. Each node is configured separately by a JSON file.

It's even possible to add an entire new hub, which can come in handy when testing on different platforms like Linux or Mac OS.

Returning to the Remote Chrome configuration, it has three other important parameters; `browserName`, `platform` and `version`. These need to comply to the node that is configured with exactly the same parameters. To view these, open the configuration for the chrome-node found in `C:/Program Files/JOSF/selenium/node-chrome.json`. By default, it looks like this

```json
{
  "capabilities": [{
    "browserName": "chrome",
    "platform": "WINDOWS",
    "version": "58",
    "maxInstances": 10
  }],

  "maxSession": 15,
  "port": 7777,
  "proxy": "org.openqa.grid.selenium.proxy.DefaultRemoteProxy",
  "register": true,
  "registerCycle": 5000,
  "hub": "http://localhost:4444",
  "nodeStatusCheckTimeout": 5000,
  "nodePolling": 5000,
  "role": "node",
  "unregisterIfStillDownAfter": 60000,
  "downPollingLimit": 2,
  "debug": false
}
```
As you see in here, it also has a `capabilities` parameter, with exactly those same parameter values as the Remote Chrome configuration has. Whenever adding any new nodes, make sure these parameter values comply, as the Selenium Server hub is mapping these.

### Capabilities

As stated earlier in this tutorial, capabilities can be configured per driver in the *driver-list.json* configuration file. An example is the `Remote Internet Explorer` driver, which has some capabilities added.

```json
{
  "displayName": "Remote Internet Explorer",
  "hubUri": "http://localhost:4444/wd/hub",
  "browserName": "internet explorer",
  "platform": "WINDOWS",
  "version": "11",
  "capabilities": {
    "ignoreProtectedModeSettings": true,
    "ignoreZoomSetting": true,
    "ie.ensureCleanSession": true
  }
}
```
These capabilities are a collection of all standard capabilities, documented by the SeleniumHQ team over at GitHub. To view all possible capabilities, go to https://github.com/SeleniumHQ/selenium/wiki/DesiredCapabilities.

### More on Selenium Server

If you'd like to know more about Selenium Server or it's configuration with JOSF, don't hesitate to [contact us](../../../about/contact.md)!