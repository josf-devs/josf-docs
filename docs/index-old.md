# Welcome to JOSF Docs

Welcome to the JOSF docs! We have released JOSF and are working hard on getting the documentation ready for you. We will frequently update this website, and if there is anything we've missed, don't hesitate to [contact us!](about/contact.md)

## Installation guide

After downloading JOSF, head over to the installation guide, to install JOSF easily.

## Configuration guide

Make sure your JOSF installation is configured for your needs!

## Tutorials

Whether you've just started dancing, or looking to sharpen those moves, go ahead to the tutorials section to get step by step guidance.

## Dance moves

JOSF consists of a lot of dance moves and it's a good idea to review the use of these cool moves! Head over to the Dance moves to see what JOSF can do!

## Technical documentation

As JOSF is open to change, we've hit you up with an open API. The packaged Drupal front-end is something we've come up with, but if a different front-end will suit your needs better, who are we to keep you from that?

### API documentation

We are working hard to complete the documentation of the API, so that you can develop against it. Please be patient and check this site regularly for updates!


