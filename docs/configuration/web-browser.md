# Configuration for Web browsers

JOSF dances with web browsers. And to connect JOSF with your web browsers, it's using Selenium Server.
Most of the configuration is covered in the ['Making some configurations' tutorial](../tutorials/versions/current/3-making-some-configurations.md), so make sure you've covered that.

## Default configuration

By default, JOSF starts a Chrome browser. This means that you need to install Chrome for this to work.
Besides an installed version of Chrome, it needs a driver to complete the connection between JOSF and Chrome;

 - Install (or update to) the [latest version of Chrome](https://www.google.com/chrome)
 - Download and extract the [latest version of the chrome webdriver](http://chromedriver.chromium.org/downloads)
 - Replace the installed version of the chrome driver, located at C:\Program Files\JOSF\api\selenium\chromediver.exe\
 - Restart JOSF to make the configuration effective.

## Adding browsers

Although for many users, the default configuration will be sufficient, JOSF is developed to orchestrate a lot of different browsers!

This is some advanced stuff, so when struggling, don't hesitate to [contact us](../about/contact.md)!

JOSF's web driver configuration is stored in the `C:\Program Files\JOSF\src\main\conf\driver-list.json`-file. Upon opening, the default shall look somewhat like this:

``` json
// The complete list of desired capabilities: https://github.com/SeleniumHQ/selenium/wiki/DesiredCapabilities
{
  "drivers": [
    {
      "displayName": "Remote Firefox",
      "hubUri": "http://localhost:4444/wd/hub",
      "browserName": "firefox",
      "platform": "WINDOWS",
      "capabilities": {}
    },
    {
      "displayName": "Remote Internet Explorer",
      "hubUri": "http://localhost:4444/wd/hub",
      "browserName": "internet explorer",
      "platform": "WINDOWS",
      "version": "11",
      "capabilities": {
        "ignoreProtectedModeSettings": true,
        "ignoreZoomSetting": true
      }
    },
    {
      "displayName": "Remote Chrome",
      "hubUri": "http://localhost:4444/wd/hub",
      "browserName": "chrome",
      "platform": "WINDOWS",
      "version": "58",
      "capabilities": {}
    }
  ]
}
```

This `json` configuration file consists of three different browsers. Adding new browsers to the stack is as simple as adding a new browser object to the json-array. Be sure to give it a clear displayName, for instance `Chrome(Linux) v40` to tell the user that it's a Chrome browser on linux, running version 40.

### A Linux Chrome example

In this example, we'll make use of a pre-installed linux version with an installed Chrome version 40 on it.

Next, the hubUri needs to point to the uri of the Selenium hub. The default Selenium hub is hosted on your own local machine, but if you'd connect to a linux machine, you can use that (ip) address. Make sure you also point to the correct port. In this example, we have a virtual machine running linux at ip `198.178.10.5` and we've exposed port `4488` to connect to the port where the Selenium Hub is listed on.

We'll set the browsername at `chrome`, platform at `linux` and version at `40`.

```json
    {
      "displayName": "Chrome(Linux) v40",
      "hubUri": "198.178.10.5:4444/wd/hub",
      "browserName": "chrome",
      "platform": "LINUX",
      "version": "40",
      "capabilities": {}
    }
```
### Configuration on the machine

First we'll configure the Selenium Hub, and after that the Chrome Selenium Node.

The hub consists of the `selenium-server-standalone.jar` file, and it's own `json` configuration file.

First, copy the selenium-server-standalone-x.x.x.jar file to the linux machine (anywhere you'd like). You can find the jar file at `C:\Program Files\JOSF\selenium`. Next, create the hub configuration json` file. The one provided in JOSF looks like this;

```json
{
  "port": 4444,
  "newSessionWaitTimeout": -1,
  "servlets" : [],
  "withoutServlets": [],
  "custom": {},
  "capabilityMatcher": "org.openqa.grid.internal.utils.DefaultCapabilityMatcher",
  "throwOnCapabilityNotPresent": true,
  "cleanUpCycle": 60,
  "role": "hub",
  "debug": true,
  "browserTimeout": 0,
  "timeout": 600,
  "host" : "localhost"
}
```

The most important configuration here, is the `"role"` option, wich is set to `"hub"`.
