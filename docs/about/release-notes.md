# Release notes

Here you can find the release notes.

## 2.12
- new feature: Configuration of proxy settings.
- new feature: Add certificates to JOSF to make use of certified HTTPS connections, when your machine does not contain the certificate by default.
- new feature: Skip SSL connection validation.
- new test step: Add data sheet. An extension of the data driven storages. Add a data sheet as a test step and select what row of data needs to be converted to variables. This selection can be exact by row name, random selected or by adding a filter to a column.
- improvement: Download web driver updates from the browser settings instantly. JOSF keeps doing this automatically on JOSF startup, but when your browsers update, you now don't need to restart JOSF to get the latest web driver updates aswell.
- small UI improvements.

## 2.11
- new feature: Add a maximum loop count to your repeating groups. Also works with `${variable}` syntax.
- improved action: the Verify node value has the tilde (~) operator which validate that the node _contains_ the given value
    - Given a node value is `hello world`, the Verify node value with data `~world` will pass as well as `~hello`.
- improvement: For the keyboard users, we've implemented some handy features to get around quickly.
- improvement: JOSF is now packaged with a compatible Java version (windows only).
- improvement: during JOSF installation, you can change your installation destination.
- fix: you can now use buffers in functions like, `${dateTime(HH:mm, +${minutes}m)}`, given `${minutes}` has a value of 3 for example.
- various minor bug fixes


## 2.10
- new feature: New conditional group option; the comparator.
    - this comparator accepts a first and second value and a set of preset comparators will conditionally execute the group.
- improvement: Actual ran test steps stored in the playbook run.
    - when using page actions, the actual ran test steps are now stored in the run, instead of a reference to the action
- new function: ${dateTime()}
    - The dateTime function aims to combine the ${today()} and ${now()} function. [Check the docs!](../actions/core/functions/date-time.md)
- some performance improvements

## 2.9
- new feature: new test step group view
    - directly run an entire group
    - convert your group into a new page action
    - fulfill buffers on group/step execution
    - drag/drop steps over different groups
- new feature: inline page-action editting:
    - when you use page actions in your tests, you can now directly edit these on-the-fly.
- new feature: inline API request/responses
    - when you run your API requests from a test, you can directly view the sent request and response inline.
- new feature: fulfledged feature editor, with;
    - syntax highlighting
    - gherkin formatting (handy for your tables: right click->format)
    - autocomplete for defined step definitions
    - directly add/edit step definitions
    - Gherkin hints and errors
- improvement: file viewer for testcases and page object models
- improvement: duplicate test cases
- improvement: sticky save button
- improvement: when objects are not found, include the used locator in the error message
- improvement: store formatted versions of objects so that comparing in your VCS works better
- overall performance improvements

## 2.8

- new feature: The dashboard is here!
    - With the first three charts in place, you have more control over the status of your application under test!
    - The results over time graph lets you zoom in to a per day result.
    - The results of the last 5 run playbook runs, give a detailed overview of the status per run.
    - And then you have the option of adding your own favorite playbooks, to keep a keen eye on those as well! 
- new function: custom date formats
  - The `${today()}` function accepts 0, 1 or 2 paramaters now.
  - The new format accepts a date format as the first paramater, with a second for adding/subtracting dates.
  - `${today(YYYY-MM-dd, 0)}` will parse 2021-03-25, given the date is 2021-03-25. The extra 0 adds no days to the current date, **but is required**
- improvement: Selenium web driver binaries update themselves. Now, when updating your browser, JOSF updates the required drivers automatically!
- improvement: A new clean look for navigation 
    - We've shifted around the navigation, so that you have a clear overview of where you are and what you can do with JOSF.
    - Also added an expand/collapse to the menu, so that you have the most usefull view for your situation.
- improvement: A new clean look for the feature overview
    - We've made some adjustments to how the feature overview looks.
    - Now it feels more like the file browser it is.
- improvement: the storage for the driver list has moved to the settings folder under testcases.
    - This especially is handy for those who want to use JOSF in CICD software, where only the testcases folder is under source control.
- Some bugs squashed
    - The breadcrumb now also works in the feature explorer
    - When debugging, your action play button for web actions is disabled to prevent unintended double actions.
    - The ambigious step screen now has a close button
    - When running a JOSF CLI command with the environment toggle, it didn't complete the testrun properly.
    - 

## 2.7

- new feature; Data driven test cases and Scenario Outline/templates
    - Make your test cases and feature files more efficient with the new data driven function
    - Feature files now support the Scenario Outline syntax to execute the same scenario with different examples
    - Test cases now support the use of Data Driven storages. Tables that include a test run name and your variables
    - Import and export to Excel or CSV, to easily share and edit the data
    - Autocompletes known variables on the data field in test cases
- new feature; Get system variable `${getEnvVar(KEY)}`
    - Especially for security purposes, you can now collect the system's environment variables instead of setting security sensitive data in JOSF
    - Given the key `USERNAME` is set as an environment variable, `${getEnvVar(USERNAME)}` will get this from the system
    - This could be used in pipeline software 
- new feature; Data generator functions
    - `${randomText()}`; Can have 0, 1 or 2 parameters; 
        - 0 parameters: gets random text with random length: `${randomText()}`
        - 1 parameter: sets the length of the text: `${randomText(7)}` - gets a random text of 7 characters
        - 2 parameters: sets the length of the text and sets it to upper or lower text: `${randomText(7, uppercase)}, ${randomText(7, lowercase)}` - gets a random text of 7 characters with uppercase or lowercase text. **note** `uppercase` can be interchanged with `upper` or `u` for short. `lowercase` can be interchanged with `lower` or `l` for short.
    - `${randomNumber()}`; Can have 0 or 2 parameters
        - 0 parameters: gets a random number
        - 2 paraters: define upper and lower boundaries: `${randomNumber(-20, 30)}` - gets a random number between these boundaries 
    - `${random()}`: Can have 0, 1 or 2 parameters
        - 0 parameters: gets a random combination of text and numbers
        - 1 parameter: gets a random combination of text and numbers with a limited amount of characters
        - 2 parameters: gets a random combination of text and numbers, and sets to upper or lower case (same as ${randomText()} function)
- new feature; JOSF CLI reporting
    - JUnit reports are generated after a playbook run
    - When the playbook run contains failed tests/scenario's, JOSF CLI exists with -1. This means that pipeline software will break on failed tests
    - A quick overview of test results will show from the JOSF CLI
- improvement; import of WSDL API definitions
- improvement; `set` action use without an object, will not open a browser anymore; handy for setting data during API tests
- further improvements;
    - Creating a new Feature file generates a template to better understand it's syntax
    - Leave page with unsaved changes directly saves (if chosen to) if you want to leave, instead of just reminding you to save
    - Importing an unkown API definition (not swagger or WSDL) will show an error now
    - Numerous UX/UI improvements


## 2.6

- new action pack; API actions; we've added an action-pack that allows the validation of API responses.
    - [`Extract node`](../actions/api/extract-node.md)
    - [`Verify node count`](../actions/api/verify-node-count.md)
    - [`Verify node present`](../actions/api/verify-node-present.md)
    - [`Verify node not present`](../actions/api/verify-node-not-present.md)
    - [`Verify node value`](../actions/api/verify-node-value.md)
    - [`Verify response time`](../actions/api/verify-response-time.md)
    - [`Verify status code`](../actions/api/verify-status-code.md)
- new function; Cloud web driver providers
    - We've enabled the use of cloud web driver providers.
    - If you'd like to test on different platforms/devices, that's now possible and easily configurable.
    - We've added SauceLabs as a possible provider
- improvement; adding local web drivers (browsers) through easy configuration
- improvement; Debugging test cases
    - You can set a break point for a specific test step in your test cases.
    - This will pause the test execution, and let's you step over the test steps one by one
- improvement; Multiple object strategies for Page Objects
    - You can add more than one strategy to your page objects
    - JOSF will combine all of these into one strategy so you can easily find objects
    - You can also tell JOSF to find not the first, but second, third, etc object it found with a specific strategy (called index)
- new object strategy; `partialID`. 
    - When using the object strategy `partialID=world`, it will find objects with the @id attribute containing the word `world`

## 2.5

- [PRO] new function; API testing. Include or import your API documentation and start testing them with JOSF PRO
    - import API documentation from Swagger or OpenAPI definition files (URLs and raw)
    - document your team's API's with the help of requests
    - use your request directly in your test cases or step definitions
    - combine API and Browser testing with the same ease-of-use you are used to in JOSF
- new function; Environments. Create different environments to quickly switch from one environment to another
    - store and use environment variables for direct use in your tests
    - minimize maintenance
- new function; Pages & Objects; Create reusable components in JOSF
    - create frequently used objects and actions and refer to them directly in your tests for reuse
    - customize page actions with input parameters
    - minimize maintenance and manage objects and actions from one place   
- improvement; Page Scanner; We've improved the page scanner, so that you can use more filters (like search on text) and made it easier to define what element you are looking for
- improvement; JOSF CLI. You can use your environment in your CLI. Open the documentation in JOSF to see how it works
- improvement; JOSF CLI. JOSF CLI now waits for the playbook run to be completed.
- improvement; meta data for test cases
- further tweaks and visual improvements to make the JOSF experience even better

## 2.4

- [PRO] new function; CLI; Run your playbooks from the CLI runner. locally or from your CI/CD pipeline solution
- new function; Playbook! Create collections of runnable test cases and features and run them all in sequence.
- new function; Tags. Tag your test cases, features and scenarios for ease of access and re-use them in a playbook.
- bugfix; some features failed to open in edit mode. Got that fixed!
- bugfix; trashed malformed step definitions threw an error. Now they don't.
- bugfix; opening new step definitions with step arguments wouldn't open. Now they do.

## 2.3

 - new function; Data tables as step arguments for gherkin features. Your step definitions can now contain six different types of data tables. ([check out our tutorial](../tutorials/versions/current/gherkin/index.md)).
 - new function; Iterate over data table rows with the new `data table repeater`.
 - new function; drap-and-drop. Why wasn't it already here... anyway, you can now change the order of groups and test steps by dragging them around!
 - new function; duplicating test steps. That's handy!
 - new function; groups for step definitions.

## 2.2

 - new group type: Conditional; Use this group to execute some steps in specific conditions!
 - new function; JOSF Settings. You can now edit:
     - the JOSF api url;
     - the default browser;
     - the default base url;
 - bugfix; in some cases, debugging your tests, didn't went as smooth as we meant;
     - pausing while in descriptive groups, exited the test run
     - when pausing and stopping a test executing, the next run continues from that point.
 - When JOSF is unable to startup, you will be notified of the reason, e.g. the current port is already in use.
 - Also, when upgrading JOSF, instead of performing any manual actions, the installation script will do that for you!

## 2.1

JOSF 2.1 comes packaged with lots of new stuff. On top of that, we've smashed some nasty bugs!

- new action; newWindow: opens new tabs within the current browser session
- new action; setPartialTextBuffer: it's possible to set a part of a text into a buffer.
- new function; groups! You can group a set of test steps, to keep your test cases neat and tidy.
- bugfix; using buffers in test objects wasn't working. Now it does.

## 2.0

JOSF 2.0 is a completely new JOSF Application, which features;

- New ways creating your test cases!
- Advanced debugging, while developing your test case!
- A new lightweight front-end Angular App as the default interface

We've replaced the Drupal front-end with an Angular app, which allowed us to deliver a light-weight JOSF experience! Our main priority remains; Test automation for everyone! That's why we've been working hard on giving you the tools to create test cases, quickly!

New debugging tools will give you the power to quickly find and automate those difficult web elements. Also, the light weight interface will allow you to navigate fluently through JOSF!

There is more to look forward! The following features will be included in future releases of JOSF.

- Writing gherkin Features (you know, just like Cucumber and SpecFlow!)
- Making use of the powerful Page Object Modal Design Pattern for your test cases.
- Chaining different types of tests and executing them using the new Play book feature.

*Note that test cases created in versions before JOSF 2.0 are not yet transferrable to the latest version of JOSF. If you need to update and migrate your current test set, please let us know by [contacting us](contact.md)!*

## 1.4

 - Added the [highlight](../actions/selenese/highlight.md) action. Useful for debugging!
 - Added the [verifyWindowTitles](../actions/selenese/verifyWindowTitles.md) action
 - Bugfix; when an empty test is run, JOSF no longer produces an error message!
 - Bugfix; when the JOSF API is unresponsive, a useful error message is shown
 - Some under the hood changes to make our work as JOSF developers more easy!


## 1.3

- Bumped version of Selenium Server from *3.4.0* to *3.11.0*
- Selenium IDE new import method for SIDE files ([check out our tutorial](../tutorials/versions/1_4/6-importing-from-selenium-ide.md)).
- Tweaked on actions
  - select
  - sendKeys
- Deprecated: authenticateAlert
- The use of dynamic dates and timestamps as test data ([check out our tutorial](../actions/core/functions/date-time.md))

## 1.2

This release pack a couple of extra actions and we've fixed some bugs!

### Actions
  - windowResize, windowMaximize
  - dragdrop, dragdropAndWait, dragAndDrop, dragAndDropAndWait, dragAndDropToObject, dragAndDropToObjectAndWait
  - createCookie
  - uncheck, uncheckAndWait
  - refresh, refreshAndWait

### Bug fixes
  - open action updated: listens to data in the test object & test data fields, simply because Selenium IDE places the data in the object field.
  - When importing invalid Selenium IDE scripts, error handling is improved by telling the erroneous line number

### Other things we did
  - Replaced the former name (testautomaat) with JOSF in most of the interfaces.

## 1.1

This release comes with a couple of extra features:

- You can now add your own test action classes to your running JOSF instance. Want to make your own actions? [Contact us](contact.md)!
- Multiple buffers (variables) per test data row is now supported
- Buffers in test objects are now supported

## 1.0

initial release