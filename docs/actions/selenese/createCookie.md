# Details
| **since**                    | *1.0*                                                        |
| ---------------------------- | ------------------------------------------------------------ |
| **action-pack**              | *Selenese*                                                   |
| **action-list.json entries** | "createCookie" : "actions.manage.Cookies", <br />"createCookieAndWait" : "actions.manage.Cookies" |
| **author**                   | *JOSF-core team*                                             |
| **status**                   | *active*                                                     |

To create a new cookie, place a name-value pair in the object field, like; name=value.

To enable more options for the cookie, add these in the data field. Currently supported options include 'path', 'max_age', 'domain', 'path' and 'isSecure'. 

The options format is "path=/path/, max_age=60, domain=.foo.com, isSecure=true". The order of options are irrelevant, the unit of the value of 'max_age' is second and the unit of the value of isSecure is a boolean. Note that specifying a domain that isn't a subset of the current domain will usually fail. Create a new cookie whose path and domain are same with those of current page under test, unless you specified a path for this cookie explicitly.

## Example

In this example, we are assuming that the base URL is *http://www.google.com*.

| Action name  | Object       | Data                                          | Result                                                  |
| ------------ | ------------ | --------------------------------------------- | ------------------------------------------------------- |
| createCookie | CNAME=MyData |                                               | Stores a new cookie named CNAME with a value of MyData. |
| createCookie | CNAME=MyData | max_age=90                                    | Also adds an expiry date of 90 seconds                  |
| createCookie | CNAME=MyData | max_age=90, isSecure=true                     | Marks the Secure boolean to true                        |
| createCookie | CNAME=MyData | max_age=90, isSecure=true, domain=.google.com | Explicitly sets the domain on .google.com               |

## Related actions

none