# Details
| **since**                  | *1.0*                            |
| -------------------------- | -------------------------------- |
| **action-pack**            | *Selenese*                       |
| **action-list.json entry** | "set" : "actions.execute.Buffer" |
| **author**                 | *JOSF-core team*                 |
| **status**                 | *active*                         |

The set action is a special action that makes the use of variables (or buffers) easy. It can be used for storing static text or text from any element for later use.

### Setting variables

The set keyword is mainly used to subtract data from elements and places them into a given variable name. These variable names get set by the value you pass in the test data field.

### Using variables

Given the variable name `MyData`, using variables can be used in the test data field like this: `${MyData}`. Of course this can be used in a sentence like;

```
I'm writing ${MyData} in here
```

Also, the use of multiple variables is supported, given that the `username` and `my site` variable are set with data;

```
Hello ${username}, welcome to ${my site}
```

Note that variable names can have spaces, to make them more readable.

## Example

In this example, we are assuming that the base URL is *http://www.google.com*.

![Google Search page](img/GoogleSearchPage.png)

| Action name | Object    | Data              | Result                                   |
| ----------- | --------- | ----------------- | ---------------------------------------- |
| set         | name=btnI | lucky button      | Sets the text "I'm Feeling Lucky" to the variable named `lucky button`. |
| type        | id=lst-ib | `${lucky button}` | Types the text "I'm Feeling Lucky" in the search bar. |

## Related actions

- [store]
- [storeEval]
- [setPartialTextBuffer](setPartialTextBuffer.md)