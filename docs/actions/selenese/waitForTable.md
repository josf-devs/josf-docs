# Details
| **since**                  | *1.2*                                  |
| -------------------------- | -------------------------------------- |
| **action-pack**            | *Selenese*                             |
| **action-list.json entry** | "waitForTable" : "actions.verify.Table" |
| **author**                 | *JOSF-core team*                       |
| **status**                 | *active*                               |

Exactly the same as [verifyTable](verifyTable.md) and [assertTable](assertTable.md)

## Related actions

- [verifyTable](assertTable.md)
- [assertTable](assertTable.md)

