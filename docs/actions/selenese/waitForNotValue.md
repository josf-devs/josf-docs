# Details
| **since**                  | *1.2*                                    |
| -------------------------- | ---------------------------------------- |
| **action-pack**            | *Selenese*                               |
| **action-list.json entry** | "waitForNotValue" : "actions.verify.Value" |
| **author**                 | *JOSF-core team*                         |
| **status**                 | *active*                                 |

Exactly the same as [verifyNotValue](verifyNotValue.md) and [assertNotValue](assertNotValue.md).

## Related actions

- [verifyNotValue](verifyNotValue.md)
- [assertNotValue](assertNotValue.md)