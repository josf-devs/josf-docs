# Details
| **since**                  | *1.2*                                  |
| -------------------------- | -------------------------------------- |
| **action-pack**            | *Selenese*                             |
| **action-list.json entry** | "verifyTable" : "actions.verify.Table" |
| **author**                 | *JOSF-core team*                       |
| **status**                 | *active*                               |

To check whether a table cell has the expected value.

## Example

A simple data table with some costumer data which has an `id` attribute with the value `customers`. The object field needs to be filled with the table location, a row number (starting from 0) and a column number (starting from 0). A few examples below.

![Table](img/Table.png)

| Action name | Object           | Data                | Result                                   |
| ----------- | ---------------- | ------------------- | ---------------------------------------- |
| verifyTable | id=customers.1.0 | Alfreds Futterkiste | Results to PASS                          |
| verifyTable | //table.5.1      | Helen Bennett       | Results to PASS                          |
| verifyTable | //table.1.0      | Helen Bennett       | Results to FAIL                          |
| verifyTable | id=customers.2.5 | Giovanni Rovelli    | Results to ERROR as the object cannot be found |

## Related actions

- [assertTable](assertTable.md)
- [waitForTable](waitForTable.md)

