# Details
| **since**                  | *1.2*                                    |
| -------------------------- | ---------------------------------------- |
| **action-pack**            | *Selenese*                               |
| **action-list.json entry** | "assertNotTable" : "actions.verify.Table" |
| **author**                 | *JOSF-core team*                         |
| **status**                 | *active*                                 |

Exactly the same as [verifyNotTable](verifyNotTable.md) and [waitForNotTable](waitForNotTable.md)

## Related actions

- [verifyNotTable](verifyNotTable.md)
- [waitForNotTable](waitForNotTable.md)


