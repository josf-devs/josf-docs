# Details
| **since**                  | *1.2*                                        |
| -------------------------- | -------------------------------------------- |
| **action-pack**            | *Selenese*                                   |
| **action-list.json entry** | "verifyEditable" : "actions.verify.Editable" |
| **author**                 | *JOSF-core team*                             |
| **status**                 | *active*                                     |

To validate if an element is editable. Elements can be text fields, checkboxes, radio buttons, drop-down list, multi-select list

## Example

A simple form with three input fields with different states:

![Simple form](img/EditableFields.png)

| Action name    | Object        | Data | Result          |
| -------------- | ------------- | ---- | --------------- |
| verifyEditable | id=f-normal   |      | Results to PASS |
| verifyEditable | id=f-disabled |      | Results to FAIL |
| verifyEditable | id=f-readonly |      | Results to FAIL |

## Related actions

- [verifyNotEditable](verifyNotEditable.md)