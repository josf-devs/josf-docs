# Details
| **since**                  | *2.1*                                             |
| -------------------------- | ------------------------------------------------- |
| **action-pack**            | *Selenese*                                        |
| **action-list.json entry** | "setPartialTextBuffer" : "actions.execute.Buffer" |
| **author**                 | *JOSF-core team*                                  |
| **status**                 | *active*                                          |

The `setPartialTextBuffer` action is a special action that makes the use of variables (or buffers) easy. It can be used for storing static text or text from any element for later use.

The difference between this and the `set` action, is that this action can store parts of text into buffers, instead of the complete text.

### Setting variables

The setPartialTextBuffer keyword is mainly used to subtract data from elements and places them into a given variable name.

## Example

In this example, we have recieved an email with a validation code to complete some registration.

``` text
Welcome to our service!

To verify your account, enter this code into your account page after your first login: 9287183.

Have a nice day!

Your Service

```

| Action name | Object    | Data              | Result                                                                  |
| ----------- | --------- | ----------------- | ----------------------------------------------------------------------- |
| setPartialTextBuffer         | className=email | first login: ${code}. | Sets `9287183` to the buffername `code` |

**N.B.**: Given that the email is set in an `<div>` element with a className of `email`.
**Note** that only one buffer gets set. The use of more than one buffer is not implemented.

## Related actions

- [set](set.md)