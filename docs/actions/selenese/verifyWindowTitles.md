# Details
| **since**                    | *1.2*                                                        |
| ---------------------------- | ------------------------------------------------------------ |
| **action-pack**              | *Selenese*                                                   |
| **action-list.json entries** | "assertAllWindowTitles" : "actions.verify.VerifyWindow"<br />"verifyAllWindowTitles" : "actions.verify.VerifyWindow"<br />"waitForAllWindowTitles" : "actions.verify.VerifyWindow"<br />"assertNotAllWindowTitles" : "actions.verify.VerifyWindow"<br />"verifyNotAllWindowTitles" : "actions.verify.VerifyWindow"<br />"waitForNotAllWindowTitles" : "actions.verify.VerifyWindow" |
| **author**                   | *JOSF-core team*                                             |
| **status**                   | *active*                                                     |

To validate if the title of the window, located in the `<title>` element of the HTML page is equal (or not in the case of -Not- actions) to the expected data.

## Example

For example the Google search page has `Google` as its title.

![Google's window title](img/windowTitle.png)

In this example, we'll use the *verify* action, but using the *assert* or *waitFor* actions, the result is the same.

| Action name              | Object | Data   | Result          |
| ------------------------ | ------ | ------ | --------------- |
| verifyAllWindowTitles    |        | Google | Results to PASS |
| verifyAllWindowTitles    |        | GogLe  | Results to FAIL |
| verifyNotAllWindowTitles |        | GogLe  | Results to PASS |
| verifyNotAllWindowTitles |        | Google  | Results to FAIL |

## Related actions

None