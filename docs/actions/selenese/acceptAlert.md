# Details
| **since**                  | *1.0*                                    |
| -------------------------- | ---------------------------------------- |
| **action-pack**            | *Selenese*                               |
| **action-list.json entry** | "acceptAlert" : "actions.execute.AlertHandle" |
| **author**                 | *JOSF-core team*                         |
| **status**                 | *active*                                 |

If a JavaScript alert (or pop-up) is present, press the accept button.

## Example

| Action name | Object | Data | Result                                   |
| ----------- | ------ | ---- | ---------------------------------------- |
| acceptAlert |        |      | The alert box is accepted and a verification is executed to make sure no other alerts are present. |

## Related actions

- [dismissAlert](dismissAlert.md)
- [getAlertText](getAlertText,md)
- [sendKeysAlert](sendKeysAlert.md)
- [authenticateAlert](authenticateAlert.md)


