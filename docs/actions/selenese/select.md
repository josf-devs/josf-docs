# Details
| **since**                  | *1.3*                                                        |
| -------------------------- | ------------------------------------------------------------ |
| **action-pack**            | *Selenese*                                                   |
| **action-list.json entry** | "select" : "actions.execute.Selection" <br /> "selectAndWait" : "actions.execute.Selection" |
| **author**                 | *JOSF-core team*                                             |
| **status**                 | *active*                                                     |

Executes a selection on either a drop-down (or combo)box or a radio button, based on the type of element that is handled. 

### Drop-down boxes

Default the selection will be on the visible text. Next to that you can also make a selection based on `label` (which is basically the same as the default selection on visible text), on `value`, `id`, and on `index`. When using the `value` option JOSF looks for a `value` element with the provided value within the options of the drop-down box . The `id` option makes it possible to select an element based on a unique `id` value which is given to the values in the drop-down box. When you use the `index` JOSF will select the value which is at the given index. The index is zero-based. 

### Radio buttons

Note that when executing select on a radio button, make sure you use the correct test data. Radio buttons are actually regular `<input>` elements with their `type` attribute set to `radio`. JOSF looks for the `value` attribute with the same value of the given test data.

## Example

For the drop-down box example, we are using [this W3Schools example](https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_select). For the radio button example we'll use [their example here](https://www.w3schools.com/html/tryit.asp?filename=tryhtml_radio).

| Action name | Object   | Data       | Result                                                       |
| ----------- | -------- | ---------- | ------------------------------------------------------------ |
| select      | //select | Opel       | Selects the *Opel* option.                                   |
| select      | //select | label=Audi | Selects the *Audi* option.                                   |
| select      | //select | index=0    | Selects the *Volvo* option.                                  |
| select      | //select | value=saab | Selects the *Saab* option. **Note:** Even if the visible text has the same value, it may not always be the case with your objects. Using the Saab (uppercase) value *will not* select the desired option. |
| select      | //input  | male       | Selects the radio button male. **Note:** Even if the label next to the radio button has the same value, it may not always be the case with your objects. Using the Male (uppercase) value *will not* select the correct radio button. |

## Related actions

- [uncheck](uncheck.md)