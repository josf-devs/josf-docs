# Details
| **since**                  | *1.2*                                           |
| -------------------------- | ----------------------------------------------- |
| **action-pack**            | *Selenese*                                      |
| **action-list.json entry** | "verifyNotEditable" : "actions.verify.Editable" |
| **author**                 | *JOSF-core team*                                |
| **status**                 | *active*                                        |

To validate if an element is not editable. Elements can be text fields, checkboxes, radio buttons, drop-down list, multi-select list

## Example

A simple form with three input fields with different states:

![Editable Fields](img/EditableFields.png)

| Action name       | Object        | Data | Result          |
| ----------------- | ------------- | ---- | --------------- |
| verifyNotEditable | id=f-normal   |      | Results to FAIL |
| verifyNotEditable | id=f-disabled |      | Results to PASS |
| verifyNotEditable | id=f-readonly |      | Results to PASS |

## Related actions

- [verifyEditable](verifyEditable.md)