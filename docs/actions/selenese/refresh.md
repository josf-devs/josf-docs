# Details
| **since**                    | *1.2*                                                        |
| ---------------------------- | ------------------------------------------------------------ |
| **action-pack**              | *Selenese*                                                   |
| **action-list.json entries** | "refresh" : "actions.execute.Refresh",<br />"refreshAndWait":"actions.execute.Refresh" |
| **author**                   | *JOSF-core team*                                             |
| **status**                   | *active*                                                     |

Simply refreshes the current window.

## Example

| Action name | Object    | Data              | Result                                   |
| ----------- | --------- | ----------------- | ---------------------------------------- |
| refresh        |  |  | The page refreshes |
| refreshAndWait        |  |  | The page refreshes |

## Related actions

None.