# Details
| **since**                  | *1.0*                                    |
| -------------------------- | ---------------------------------------- |
| **action-pack**            | *Selenese*                               |
| **action-list.json entry** | "verifyNotSelected" : "actions.verify.Selected" |
| **author**                 | *JOSF-core team*                         |
| **status**                 | *active*                                 |

Verifying whether a select box does not have the expected value, can be done by the verifyNotSelected action.

## Example

A simple form with a select box.

![Select box](img/SelectBox.png)

| Action name       | Object   | Data  | Result          |
| ----------------- | -------- | ----- | --------------- |
| verifyNotSelected | //select | Volvo | Results to FAIL |
| verifyNotSelected | //select | Opel  | Results to PASS |

## Related actions

- [select](select.md)