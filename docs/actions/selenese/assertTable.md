# Details
| **since**                  | *1.2*                                  |
| -------------------------- | -------------------------------------- |
| **action-pack**            | *Selenese*                             |
| **action-list.json entry** | "assertTable" : "actions.verify.Table" |
| **author**                 | *JOSF-core team*                       |
| **status**                 | *active*                               |

Exactly the same as [verifyTable](verifyTable.md) and [waitForTable](waitForTable.md)

## Related actions

- [verifyTable](verifyTable.md)
- [waitForTable](waitForTable.md)

