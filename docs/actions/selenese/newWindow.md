# Details
| **since**                  | *2.1*                                    |
| -------------------------- | ---------------------------------------- |
| **action-pack**            | *Selenese*                               |
| **action-list.json entry** | "newWindow" : "actions.manage.NewWindow" |
| **author**                 | *JOSF-core team*                         |
| **status**                 | *active*                                 |

To open a second (or third, of fourth) tab within your browser session, use the **newWindow** command.

The newWindow command accepts a relative or absolute URL as parameter. This can be placed in either the Object or the Data field.

## Example

| Action name | Object                 | Data                   | Result                                           |
| ----------- | ---------------------- | ---------------------- | ------------------------------------------------ |
| newWindow   |                        |                        | a new tab will be open on the baseUrl            |
| newWindow   | http://www.google.com/ |                        | a new tab will be open on http://www.google.com/ |
| newWindow   |                        | http://www.google.com/ | a new tab will be open on http://www.google.com/ |

## Related actions

- [close](close.md)
- [open](open.md)


