# Details
| **since**                  | *1.2*                                       |
| -------------------------- | ------------------------------------------- |
| **action-pack**            | *Selenese*                                  |
| **action-list.json entry** | "windowMaximize" : "actions.execute.Resize" |
| **author**                 | *JOSF-core team*                            |
| **status**                 | *active*                                    |

To maximize the browser window.

## Example

| Action name    | Object | Data    | Result                                                      |
| -------------- | ------ | ------- | ----------------------------------------------------------- |
| windowMaximize |        |         | Browser window dimensions maximized.                        |
| windowMaximize |        | 400x300 | Browser window dimensions maximized; test data are ignored. |

## Related actions

- [windowResize](windowResize.md)

