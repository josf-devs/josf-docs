# Details
| **since**                  | *1.0*                                    |
| -------------------------- | ---------------------------------------- |
| **action-pack**            | *Selenese*                               |
| **action-list.json entry** | "getAlertText" : "actions.execute.AlertHandle" |
| **author**                 | *JOSF-core team*                         |
| **status**                 | *active*                                 |

If a JavaScript alert (or pop-up) is present, the given *data* is verified against the text in the pop-up.

## Example

| Action name  | Object | Data                | Result                                   |
| ------------ | ------ | ------------------- | ---------------------------------------- |
| getAlertText |        | Welcome to our site | Verifies if the text from the pop-up contains the text "Welcome to our site". |

## Related actions

- [dismissAlert](dismissAlert.md)
- [acceptAlert](acceptAlert.md)
- [sendKeysAlert](sendKeysAlert.md)
- [authenticateAlert](authenticateAlert.md)

