# Details
| **since**                  | *1.2*                                   |
| -------------------------- | --------------------------------------- |
| **action-pack**            | *Selenese*                              |
| **action-list.json entry** | "waitForValue" : "actions.verify.Value" |
| **author**                 | *JOSF-core team*                        |
| **status**                 | *active*                                |

Exactly the same as [verifyValue](verifyValue.md) and [assertValue](assertValue.md).

## Related actions

- [verifyValue](verifyValue.md)
- [assertValue](assertValue.md)