# Details
| **since**                  | *1.0*                                               |
| -------------------------- | --------------------------------------------------- |
| **action-pack**            | *Selenese*                                          |
| **action-list.json entry** | "authenticateAlert" : "actions.execute.AlertHandle" |
| **author**                 | *JOSF-core team*                                    |
| **status**                 | *deprecated since version 1.3*                      |

## Deprecated

This function always has been in beta, and never fully implemented in the WebDriver standard. Since the use of Selenium 3.11.0, this action is no longer supported.

### Work around

When visiting a website that has an authentication pop-up, use the username and password in the baseUrl like this:

```
    http://username:password@www.mywebsite.com/
```

Most of the modern browsers support this type of HTTP authentication.

## Documentation for version below 1.3

If a JavaScript authentication alert (or pop-up) is present, the username and password are used to enter the credentials.

![JavaScript authentication popup](img/auth-alert.png)

## Example

| Action name       | Object | Data               | Result                                                            |
| ----------------- | ------ | ------------------ | ----------------------------------------------------------------- |
| authenticateAlert |        | username\|password | The pipe-symbol **\|** is used to separate username and password. |

Any spaces around the pipe-symbol **will** be part of either the username or password, e.g. "admin | Passw0rd!" will result in a username: "admin " and password: " Passw0rd!"

## Related actions

- [dismissAlert](dismissAlert.md)
- [acceptAlert](acceptAlert.md)
- [getAlertText](getAlertText.md)
- [sendKeysAlert](sendKeysAlert.md)

