# Details
| **since**                    | *1.2*                                                        |
| ---------------------------- | ------------------------------------------------------------ |
| **action-pack**              | *Selenese*                                                   |
| **action-list.json entries** | "uncheck" : "actions.execute.Uncheck",<br />"uncheckAndWait":"actions.execute.Uncheck" |
| **author**                   | *JOSF-core team*                                             |
| **status**                   | *active*                                                     |

Unchecks checkboxes and radio buttons.

**Note** that usually a User is not able to uncheck a radio-button. JOSF includes this function mainly because Selenium IDE integrated this function to make your testing easier. Also note that this relies on JavaScript to be run within the browser you are using.

## Example

| Action name | Object    | Data              | Result                                   |
| ----------- | --------- | ----------------- | ---------------------------------------- |
| uncheck  | id=some-checked-radio |  | The radio button will be unchecked. |
| uncheck  | id=some-unchecked-radio |  | The radio button stays unchecked, but a warning will be reported, stating the already unchecked object. |
| uncheck  | id=some-checked-checkbox |  | The checkbox will be unchecked. |
| uncheck  | id=some-unchecked-checkbox |  | The checkbox stays unchecked, but a warning will be reported, stating the already unchecked object. |

## Related actions

- [select](select.md)