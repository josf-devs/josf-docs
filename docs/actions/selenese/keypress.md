# Details
| **since**                  | *1.0*                                   |
| -------------------------- | --------------------------------------- |
| **action-pack**            | *Selenese*                              |
| **action-list.json entry** | "keypress" : "actions.execute.KeyPress" |
| **author**                 | *JOSF-core team*                        |
| **status**                 | *active*                                |

The keypress action presses keys that are not very usual, like a TAB or ENTER key. A list of all [possible keys is found here](https://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/index_exports_Key.html). The key will be executed against the given object. Multiple keypresses can be separated by spaces. 

## Example

In this example, we are assuming that the base URL is *http://www.google.com*.

![Google search page](img/GoogleSearchPage.png)

| Action name | Object    | Data              | Result                                   |
| ----------- | --------- | ----------------- | ---------------------------------------- |
| type        | id=lst-ib | This is some text | To type some text in the search field.   |
| keypress    | id=lst-ib | ENTER             | Executes an Enter key press on the search field, wich results in submitting an enter |
| keypress    | id=lst-ib | TAB TAB TAB ENTER | Performs a TAB keypress three times in a row on the same element and finally presses ENTER on the element. |

### Configurable item

Between each keypress, JOSF waits for it to complete for a fixed amount of time. By default, this is 1 second (or 1000 milliseconds), but can be changed by editing the `pauseBetweenKeys` option in the `config.json` file. If this is not present, it can be added by;

```json
"pauseBetweenKeys" : 2500
```

Note that the value is set in milliseconds, so 2500 translates to 2,5 seconds.

## Related actions

None.