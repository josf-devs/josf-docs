# Details
| **since**                  | *1.2*                                  |
| -------------------------- | -------------------------------------- |
| **action-pack**            | *Selenese*                             |
| **action-list.json entry** | "assertValue" : "actions.verify.Value" |
| **author**                 | *JOSF-core team*                       |
| **status**                 | *active*                               |

Exactly the same as [verifyNotValue](verifyNotValue.md) and [waitForNotValue](waitForNotValue.md).

## Related actions

- [verifyNotValue](verifyNotValue.md)
- [waitForNotValue](waitForNotValue.md)