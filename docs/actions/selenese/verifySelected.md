# Details
| **since**                  | *1.0*                                    |
| -------------------------- | ---------------------------------------- |
| **action-pack**            | *Selenese*                               |
| **action-list.json entry** | "verifySelected" : "actions.verify.Selected" |
| **author**                 | *JOSF-core team*                         |
| **status**                 | *active*                                 |

Verifying whether a select box has the expected value, can be done by the verifySelected action.

## Example

A simple form with a select box.

![Select box](img/SelectBox.png)

| Action name    | Object   | Data  | Result          |
| -------------- | -------- | ----- | --------------- |
| verifySelected | //select | Volvo | Results to PASS |
| verifySelected | //select | Opel  | Results to FAIL |

## Related actions

- [select](select.md)