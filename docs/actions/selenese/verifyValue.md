# Details
| **since**                  | *1.2*                                  |
| -------------------------- | -------------------------------------- |
| **action-pack**            | *Selenese*                             |
| **action-list.json entry** | "verifyValue" : "actions.verify.Value" |
| **author**                 | *JOSF-core team*                       |
| **status**                 | *active*                               |

To validate the value of any `input` element.

## Example

A simple form with two input fields.

![Simple form](img/simpleForm.png)

| Action name | Object                  | Data        | Result                                   |
| ----------- | ----------------------- | ----------- | ---------------------------------------- |
| verifyValue | name=lname              | John        | Results to PASS                          |
| verifyValue | name=lname              | Jo          | Results to FAIL                          |
| verifyValue | //input[@type='submit'] | Submit form | Results to PASS as it goes for all `input` elements that have a `value` attribute. |

## Related actions

- [assertValue](assertValue.md)
- [waitForValue](waitForValue.md)

