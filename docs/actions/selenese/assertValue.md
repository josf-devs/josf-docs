# Details
| **since**                  | *1.2*                                  |
| -------------------------- | -------------------------------------- |
| **action-pack**            | *Selenese*                             |
| **action-list.json entry** | "assertValue" : "actions.verify.Value" |
| **author**                 | *JOSF-core team*                       |
| **status**                 | *active*                               |

Exactly the same as [verifyValue](verifyValue.md) and [waitForValue](waitForValue.md).

## Related actions

- [verifyValue](verifyValue.md)
- [waitForValue](waitForValue.md)