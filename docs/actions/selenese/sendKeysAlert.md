# Details
| **since**                  | *1.0*                                    |
| -------------------------- | ---------------------------------------- |
| **action-pack**            | *Selenese*                               |
| **action-list.json entry** | "sendKeysAlert" : "actions.execute.AlertHandle" |
| **author**                 | *JOSF-core team*                         |
| **status**                 | *active*                                 |

If a JavaScript alert (or pop-up) is present, the given *data* is send to the popup text box and the alert gets accepted.

## Example

| Action name   | Object | Data      | Result                                   |
| ------------- | ------ | --------- | ---------------------------------------- |
| sendKeysAlert |        | Some text | Sends the text "Some text" to the popup and accepts the popup. |

## Related actions

- [dismissAlert](dismissAlert.md)
- [acceptAlert](acceptAlert.md)
- [getAlertText](getAlertText.md)
- [authenticateAlert](authenticateAlert.md)


