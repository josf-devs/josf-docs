# Details
| **since**                  | *1.0*                               |
| -------------------------- | ----------------------------------- |
| **action-pack**            | *Selenese*                          |
| **action-list.json entry** | "submit" : "actions.execute.Submit" |
| **author**                 | *JOSF-core team*                    |
| **status**                 | *active*                            |

When in a form, the submit action performs a submit on the form. The element in the object field should be any element present in the form. This is often the submit button, but the example below shows the use of submit on an `input` field.

## Example

In this example, we are assuming that the base URL is *http://www.google.com*.

![Google search page](img/GoogleSearchPage.png)

| Action name | Object    | Data              | Result                                   |
| ----------- | --------- | ----------------- | ---------------------------------------- |
| type        | id=lst-ib | This is some text |                                          |
| submit      | id=lst-ib |                   | Performs the submit on the text input field. The form will be submitted and the Google search will be executed. |

## Related actions

None.