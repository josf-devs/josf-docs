# Details
| **since**                  | *1.2*                                    |
| -------------------------- | ---------------------------------------- |
| **action-pack**            | *Selenese*                               |
| **action-list.json entry** | "verifyNotValue" : "actions.verify.Value" |
| **author**                 | *JOSF-core team*                         |
| **status**                 | *active*                                 |

To validate a value of any `input` for not being the expected.

## Example

A simple form with two input fields.

![Simple form](img/simpleForm.png)

| Action name    | Object                  | Data               | Result                                   |
| -------------- | ----------------------- | ------------------ | ---------------------------------------- |
| verifyNotValue | name=lname              | Jo                 | Results to PASS                          |
| verifyNotValue | name=lname              | John               | Results to FAIL                          |
| verifyNotValue | //input[@type='submit'] | Submit form BUTTON | Results to PASS as it goes for all `input` elements that have a `value` attribute. |

## Related actions

- [assertNotValue](assertNotValue.md)
- [waitForNotValue](waitForNotValue.md)