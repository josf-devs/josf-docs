# Details
| **since**                  | *1.5*                                    |
| -------------------------- | ---------------------------------------- |
| **action-pack**            | *Selenese*                               |
| **action-list.json entry** | "deleteCookie" : "actions.manage.Cookie" |
| **author**                 | *JOSF-core team*                         |
| **status**                 | *active*                                 |

Delete a named cookie with specified path and domain. Be careful; to delete a cookie, you need to delete it using the exact same path and domain that were used to create the cookie. If the path is wrong, or the domain is wrong, the cookie simply won't be deleted. Also note that specifying a domain that isn't a subset of the current domain will usually fail. Since there's no way to discover at runtime the original path and domain of a given cookie, we've added an option called 'recurse' to try all sub-domains of the current domain with all paths that are a subset of the current path. Beware; this option can be slow. In big-O notation, it operates in O(n*m) time, where n is the number of dots in the domain name and m is the number of slashes in the path.

**Arguments:**

 - Object - the name of the cookie to be deleted
 - Data - options for the cookie. Currently supported options include 'path', 'domain' and 'recurse.' The optionsString's format is "path=/path/, domain=.foo.com, recurse=true". The order of options are irrelevant. Note that specifying a domain that isn't a subset of the current domain will usually fail.

## Example

In this example, we are assuming that the base URL is *http://www.google.com*.

| Action name  | Object   | Data                                       | Result                                   |
| ------------ | -------- | ------------------------------------------ | ---------------------------------------- |
| deleteCookie |          |                                            | Does nothing                             |
| deleteCookie |          | path=/,domain=.google.com                  | Deletes cookie based on path and domain. |
| deleteCookie | MyCookie |                                            | Deletes cookie based on the name.        |
| deleteCookie |          | path=/maps,domain=.google.com,recurse=true | Recursively removes cookies. See below.  |

### Recurse

The `recurse` option will try and remove all cookies on the path. Given the following options string; `path=/my/subsite,domain=.website.co.uk`. This will remove all cookies that have these path/domain combinations.

**Options string**

 - path=/my,domain=website,recurse=false
 - path=/my,domain=co,recurse=false
 - path=/my,domain=uk,recurse=false
 - path=/subsite,domain=website,recurse=false
 - path=/subsite,domain=co,recurse=false
 - path=/subsite,domain=uk,recurse=false

