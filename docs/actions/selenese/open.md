# Details
| **since**                  | *1.0*                              |
| -------------------------- | ---------------------------------- |
| **action-pack**            | *Selenese*                         |
| **action-list.json entry** | "open" : "actions.execute.GetPage" |
| **author**                 | *JOSF-core team*                   |
| **status**                 | *active*                           |

The open action *opens* websites and sub-pages of the base URL. Note that since JOSF 1.2, it's also possible to enter the requested page into the object field. Note that the object field will get precedence over the data field.

## Example

In this example, we are assuming that the base URL is *http://www.google.com*.

| Action name | Object                    | Data                            | Result                                   |
| ----------- | ------------------------- | ------------------------------- | ---------------------------------------- |
| open        |                           | /maps                           | Opens the webpage http://www.google.com/maps |
| open        |                           | http://www.other-site.com       | Opens the website http://www.other-site.com |
| open        | /maps                     |                                 | Opens the webpage http://www.google.com/maps |
| open        | http://www.other-site.com |                                 | Opens the website http://www.other-site.com |
| open        | /maps                     | http://www.other-site.com       | Opens the website http://www.google.com/maps as object has precedence over data. |
| open        | /maps                     | I want to open the maps webpage | Opens the website http://www.google.com/maps and does nothing with the data field. |

## Related actions

None.