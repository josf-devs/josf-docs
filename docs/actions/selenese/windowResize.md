# Details
| **since**                  | *1.2*                                     |
| -------------------------- | ----------------------------------------- |
| **action-pack**            | *Selenese*                                |
| **action-list.json entry** | "windowResize" : "actions.execute.Resize" |
| **author**                 | *JOSF-core team*                          |
| **status**                 | *active*                                  |

To resize the browser window to a specific dimension. Your input of desired dimension must meet one of the following regex formats:

- `\d+x\d+` - to set width and height of the browser window.
- `\d+,\d+` - to set width and height of the browser window.
- `\d+` - to set width only, height stays unchanged.

To maximize the browser window the data word `fullscreen` can be used. 

## Example

| Action name  | Object | Data        | Result                                                       |
| ------------ | ------ | ----------- | ------------------------------------------------------------ |
| windowResize |        | 1200x800    | Browser window width set to 1200 px and height set to 800 px. |
| windowResize |        | 500,400     | Browser window width set to 500 px and height set to 400 px. |
| windowResize |        | 1280        | Browser width set to 1280 px, height unchanged.              |
| windowResize |        | fullscreen  | Browser window dimensions maximized.                         |
| windowResize |        |             | ERROR Unexpected format because test data is required.       |
| windowResize |        | 800 by 600  | ERROR Unexpected format because the test data does not match with one of the allowed regex formats. |
| windowResize |        | 50000x30000 | WARNING Unable to set desired window size. Your browser window will be maximized (unless you have a device that can really work with this resolution). |

## Related actions

- [windowMaximize](windowMaximize.md)

