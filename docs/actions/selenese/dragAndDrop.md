# Details
| **since**                    | *1.2*                                    |
| ---------------------------- | ---------------------------------------- |
| **action-pack**              | *Selenese*                               |
| **action-list.json entries** | "dragdrop" : actions.execute.DragAndDrop", <br />"dragdropAndWait" : "actions.execute.DragAndDrop",<br />"dragAndDrop" : "actions.execute.DragAndDrop", <br />"dragAndDropAndWait" : "actions.execute.DragAndDrop", <br />"dragAndDropToObject" : "actions.execute.DragAndDrop", <br />"dragAndDropToObjectAndWait" : "actions.execute.DragAndDrop", |
| **author**                   | *JOSF-core team*                         |
| **status**                   | *active*                                 |

The drag and drop action takes one object, drags that, and drops it at another object, or at an offset from the given object. This action listens to multiple keywords.


## Example

In this example, we are assuming that the base URL is *http://jqueryui.com/droppable/*.
**Note:** The drapAndDrop action name can be exchanged for any of the drad and drop keywords (see table above).

| Action name | Object               | Data                 | Result                                   |
| ----------- | -------------------- | -------------------- | ---------------------------------------- |
| dragAndDrop | //*[@id='draggable'] | 200, 0               | Drags the object 200 pixels over the x-axis (to the right). |
| dragAndDrop | //*[@id='draggable'] | 0, 200               | Drags the object 200 pixels over the y-axis (to the bottom). |
| dragAndDrop | //*[@id='draggable'] | -200, 0              | Drags the object 200 pixels over the x-axis (to the left). |
| dragAndDrop | //*[@id='draggable'] | 0, -200              | Drags the object 200 pixels over the y-axis (to the top). |
| dragAndDrop | //*[@id='draggable'] | 200, -200            | Drags the object 200 pixels over the x-axis (to the right) and 200 pixels over the y-axis (to the top). |
| dragAndDrop | //*[@id='draggable'] | 200, -200            | Drags the object 200 pixels over the x-axis (to the right) and 200 pixels over the y-axis (to the top). |
| dragAndDrop | //*[@id='draggable'] | //*[@id='droppable'] | Drags the object over to the `droppable` object. |

Also note that this action will *assert* that the object indeed moved. If it didn't change position, it will retry the action untill it A) moves or B) takes too long and throws an error.

## Related actions

none.
