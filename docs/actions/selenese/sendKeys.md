# Details
| **since**                  | *1.3*                                                        |
| -------------------------- | ------------------------------------------------------------ |
| **action-pack**            | *Selenese*                                                   |
| **action-list.json entry** | "sendKeys" : "actions.execute.SendKeys"<br /> "sendKeysAndWait" : "actions.execute.SendKeys" |
| **author**                 | *JOSF-core team*                                             |
| **status**                 | *active*                                                     |

With this action you can simulate a real user typing a string into a specified input field. Just like a real user, this action cannot type into fields which are disabled, read only or invisible. The text you send will not replace any existing content, but will be added to any existing content.

## Example

| Action name | Object    | Data              | Result                                                       |
| ----------- | --------- | ----------------- | ------------------------------------------------------------ |
| sendKeys    | id=lst-ib | This is some text | Types the text "This is some text" in the object that has an ID *lst-ib*. |

## Related actions

- [type](type.md)
- [keypress](keypress.md)


