# xDetails
| **since**                  | *1.0*                           |
| -------------------------- | ------------------------------- |
| **action-pack**            | *Selenese*                      |
| **action-list.json entry** | "type" : "actions.execute.Type" |
| **author**                 | *JOSF-core team*                |
| **status**                 | *active*                        |

The type action sends keystrokes to input fields and text areas. Note that before typing text, the element will be cleared form any present text.

## Example

In this example, we are assuming that the base URL is *http://www.google.com*.

| Action name | Object    | Data              | Result                                   |
| ----------- | --------- | ----------------- | ---------------------------------------- |
| type        | id=lst-ib | This is some data | Types the text "This is some data" in the object that has an ID *lst-ib* |

## Related actions

- keypress

