# Details
| **since**                  | *1.2*                                    |
| -------------------------- | ---------------------------------------- |
| **action-pack**            | *Selenese*                               |
| **action-list.json entry** | "waitForNotTable" : "actions.verify.Table" |
| **author**                 | *JOSF-core team*                         |
| **status**                 | *active*                                 |

Exactly the same as [verifyNotTable](verifyNotTable.md) and [assertNotTable](assertNotTable.md)

## Related actions

- [verifyNotTable](assertNotTable.md)
- [assertNotTable](assertNotTable.md)

