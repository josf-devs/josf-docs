# Details
| **since**                  | *1.4*                                     |
| -------------------------- | ----------------------------------------- |
| **action-pack**            | *Selenese*                                |
| **action-list.json entry** | "highlight" : "actions.execute.Highlight" |
| **author**                 | *JOSF-core team*                          |
| **status**                 | *active*                                  |

Briefly changes the backgroundColor of the specified element yellow. Useful for debugging.

## Example

Given we are on the Google search page

| Action name  | Object | Data                | Result                                   |
| ------------ | ------ | ------------------- | ---------------------------------------- |
| highlight | id=lst-ib       | | Sets the background color of the given object to yellow, and after 200 milliseconds, changes it to the original background color |

## Related actions

none