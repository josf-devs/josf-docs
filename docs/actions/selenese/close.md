# Details
| **since**                  | *1.2*                            |
| -------------------------- | -------------------------------- |
| **action-pack**            | *Selenese*                       |
| **action-list.json entry** | "close" : "actions.manage.Close" |
| **author**                 | *JOSF-core team*                 |
| **status**                 | *active*                         |

The close action closes the active window like a user would click on the close button of a popup or a tab. Make sure you switch to the correct window first.

## Example

In this example, we are assuming that the base URL is *http://www.google.com*.

| Action name | Object | Data | Result                           |
| ----------- | ------ | ---- | -------------------------------- |
| close       |        |      | The webpage (and browser) closes |

## Related actions

None.