# Details
| **since**                  | *1.0*                                    |
| -------------------------- | ---------------------------------------- |
| **action-pack**            | *Selenese*                               |
| **action-list.json entry** | "clickAndWait" : "actions.execute.Click" |
| **author**                 | *JOSF-core team*                         |
| **status**                 | *active*                                 |

The clickAndWait action clicks at objects, based on the object. When clicking on an object fails (e.g. when another element is in front of it), a JavaScript action will kick in to click on the given object.

**Note:** This does exactly the same as the regular [*click*](click.md) action.

## Example

In this example, we are assuming that the base URL is *http://www.google.com*.

| Action name  | Object    | Data | Result                                   |
| ------------ | --------- | ---- | ---------------------------------------- |
| clickAndWait | name=btnK |      | Opens the webpage http://www.google.com/maps |

**Tip**: You can use the data field, to place some text to remind yourself what you are clicking. JOSF won't use the data field for anything.

## Related actions

- [click](click.md)

