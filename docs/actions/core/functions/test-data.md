# Details
| **since**       | 2.7              |
| --------------- | ---------------- |
| **action-pack** | *Core*           |
| **author**      | *JOSF-core team* |
| **status**      | *active*         |

Built-in functions to generate test data.

Functions are always written just like variables: `${myKeyword}`. Except that functions have parentheses at the end of the function name: `()`. These parentheses can hold parameters to add more functionality to the functions.

## bsn()

Returns a BSN-number that meets the criteria for a valid BSN-number.

*A BSN-number is a personal identification number in the Netherlands, provided by the Dutch government.*

### Parameter list

The `bsn()` function does not have any parameters:

`bsn()`

| Example  | Possible result |
| -------- | --------------- |
| ${bsn()} | 127377645       |



## iban()

Returns an IBAN-bank account number that meets the criteria for a valid IBAN-number. At the moment only Dutch IBAN-numbers can be generated.

### Parameter list

The `random()` function accepts two parameters to format the outcome:

`iban(bankcode, formatted)`

| Parameter | Description              | Valid values        |
| --------- | ------------------------ | ------------------- |
| bankcode  | The bankcode to be used. | String of 4 letters |
| formatted | Returns a formatted IBAN | f                   |

Default the bankcode TEST is used and no formatting is applied.



| Example          | Possible result                                              |
| ---------------- | ------------------------------------------------------------ |
| ${iban()}        | NL44TEST0675748720                                           |
| ${iban(INGB)}    | NL51INGB0159598052                                           |
| ${iban(f)}       | NL66 TEST 0755 9879 85                                       |
| ${iban(abna, f)} | NL13 ABNA 0940 7927 53                                       |
| ${iban(ABN2, F)} | NL77 TEST 0078 5908 09 (provided bankcode is invalid, default is used) |



## random()

Returns a random string of letters and numbers.

### Parameter list

The `random()` function accepts a couple of parameters to format the outcome:

`random(length, casing)`

| Parameter | Description | Valid values |
| - | - | - |
| length | Defines the number of characters in the string. If omitted the string will have a default length of 10. | > 0 |
| casing | Defines if the letters in the string should be uppercase of lowercase. If omitted both uppercase and lowercase letters can occur in the string. <br />Casing must be the second parameter. This means that it can only be used when the length is also provided. | u<br />upper<br />uppercase<br />l<br />lower<br />lowercase |



| Example                  | Possible result                                  |
| ------------------------ | ------------------------------------------------ |
| ${random()}              | ulgI6AdxZB                                       |
| ${random(5)}             | 451iG                                            |
| ${random(15, u)}         | DKJBEJWD8IG8MTS                                  |
| ${random(13, lowercase)} | qztc5bqd5cmxutb                                  |
| ${random(uppercase)}     | *Error* (the first parameter must be the length) |



## randomText()

Works the same as the `random()` function but only returns letters, not numbers.

`randomText(length, casing)`

| Example                  | Possible result                                  |
| ------------------------ | ------------------------------------------------ |
| ${randomText()}          | TbVxGoIOai                                       |
| ${randomText(5)}         | jrAFa                                            |
| ${randomText(15, upper)} | HCBDOJTSATSZNYM                                  |
| ${randomText(13, l)}     | vbeixcrvrduns                                    |
| ${randomText(uppercase)} | *Error* (the first parameter must be the length) |



## randomNumber()

Returns a random number (integer).

### Parameter list

The `randomNumber()` function accepts 0 or 2 parameters to steer the outcome:

`randomNumber(lowerLimit, upperLimit)`

| Parameter  | Description                       | Valid values                                 |
| ---------- | --------------------------------- | -------------------------------------------- |
| lowerLimit | The minimum number to be returned | Valid integer values (positive and negative) |
| upperLimit | The maximum number to be returned | Valid integer values (positive and negative) |



| Example                    | Possible result                                              |
| -------------------------- | ------------------------------------------------------------ |
| ${randomNumber()}          | 449566771 (only positive numbers will be returned)           |
| ${randomNumber(5, 10)}     | 8                                                            |
| ${randomNumber(-500, 500)} | -387                                                         |
| ${randomNumber(5)}         | *Error* (if limits are provided both the lower limit and the upper limit must be given) |
| ${randomText(11, 11)}      | 11                                                           |



## rrn()

Returns a Belgian Rijksregisternummer that meets the criteria for a valid RRN-number. 

*A RRN-number is a personal identification number in Belgium, provided by the Belgian government.*

### Parameter list

The `rrn()` function accepts two optional parameters to format the outcome:

`iban(dateOfBirth, gender)`

| Parameter   | Description       | Valid values |
| ----------- | ----------------- | ------------ |
| dateOfBirth | The date of birth | Valid dates  |
| gender      | Gender            | f, m         |

When no (valid) `dateOfBirth` is provided the current date is used as the default. 

When no (valid) `gender` is provided the gender is set to `f` (female) as the default.



| Example               | Possible result |
| --------------------- | --------------- |
| ${rrn()}              | 21032559420     |
| ${rrn(25-06-1995)}    | 95062576042     |
| ${rrn(25-06-1995, m)} | 95062580101     |
| ${rrn(m)}             | 21032592973     |
| ${rrn(31-02-2001)}    | 01022811678     |
| ${rrn(32-02-2001)}    | 21032557440     |



