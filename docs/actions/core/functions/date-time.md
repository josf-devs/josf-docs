# Details
| **since**       | *1.3*            |
|-----------------|------------------|
| **action-pack** | *Core*           |
| **author**      | *JOSF-core team* |
| **status**      | *active*         |

Built-in functions to get the current date or time.

Functions are always written just like variables: `${myKeyword}`. Except that functions have parentheses at the end of the function name: `()`. These parentheses can hold parameters to add more functionality to the functions.

## Deprecation notice

The following functions have been deprecated since version 2.10;

 - ${today()}
 - ${today(modifier)}
 - ${today(format, modifier)}
 - ${now()}
 - ${now(modifier)}

These functions have been replaced by the *new* dateTime function.

## dateTime()

The dateTime function accepts 0, 1 or 2 paramters;

 - ${dateTime()}
 - ${dateTime(format)}
 - ${dateTime(format, modifier)}

It replaces the today() and now() function, as the dateTime() accepts date and time formatting options.

### Examples

If any paramters are ommitted, JOSF takes the *configuration option* FormatDate to format your date. By default, this is set to `YYYY-MM-dd`.

#### Format options

| Format                                  | Result              |
|-----------------------------------------|---------------------|
| ${dateTime(YYYY-MM-dd)}                 | 2018-05-24          |
| ${dateTime(YY.dd.MM)}                   | 18.24.05            |
| ${dateTime(YYYY-MM-dd'T'HH:ss:mm, +1y)} | 2019-05-24T20:43:33 |
| ${dateTime(HH:ss)}                      | 20:43               |
| ${dateTime(HH:ss, -1H)}                 | 19:43               |

Please check out the [table bellow](#formatting-date-time) for a total list of formatting options.

#### Modifier options

A modifier should always come in sets of three;
```none
                      +2d
      add or subtract ↲↓↳ the unit
                    the number
```
 | Mofidier  | result                            |
 |-----------|-----------------------------------|
 | +20 d     | add 20 days                       |
 | -19 Y     | subtract 19 years                 |
 | +2 M -8 Y | add 2 months AND subtract 8 years |

Note that spaces are allowed for better readability.

The options for the unit are;

- y, or Y - for YEARS
- M - for MONTHS (note the capitalized M)
- d - for DAYS
- H - for HOURS
- m - for MINUTES (note the non-capitalized m)
- s - for SECONDS

## today() - *DEPRECATED*

**WARNING: THIS FUNCTION IS DEPCRECATED SINCE VERSION 2.10 AND MAY BE REMOVED IN FUTURE RELEASES. PLEASE MAKE USE OF `dateTime()` INSTEAD**

Returns the current date. For formatting, review the Configuration section.

### Parameter list

The `today()` function accepts a couple of parameters to add or subtract values from todays date.  

Given the current date is 24-05-2018.

| Symbol | Description           | Example    | Result     |
|--------|-----------------------|------------|------------|
| y      | add/subtract # years  | today(+1y) | 24-05-2019 |
| m      | add/subtract # months | today(+1m) | 24-06-2018 |
| w      | add/subtract # weeks  | today(+1w) | 31-05-2018 |
| d      | add/subtract # days   | today(+1d) | 25-05-2018 |

You can also change the way JOSF parses the date to other formats. 
This requires to set two parameters, the format and the addintion/subtraction, which can also be **0**.

| Format                    | Result     |
|---------------------------|------------|
| ${today(YYYY-MM-dd, 0)}   | 2018-05-24 |
| ${today(YY.dd.MM, 0)}     | 18.24.05   |
| ${today(YYYY-MM-dd, +1y)} | 2019-05-24 |
| ${today(YY.dd.MM, -1y)}   | 17.24.05   |

A more complex, but valid use case would be: `today(+1w-2d)` wich translates to today plus 1 week, minus 2 days. Off course `today(+5d)` would also work just fine. Even `today(+1y-40d+12w)` would return a valid date.

## now()  - *DEPRECATED*

**WARNING: THIS FUNCTION IS DEPCRECATED SINCE VERSION 2.10 AND MAY BE REMOVED IN FUTURE RELEASES. PLEASE MAKE USE OF `dateTime()` INSTEAD**

Returns the current time. For formatting, review the Configuration section.

### Parameter list

The `now()` function accepts a couple of parameters to add or subtract values from this moment in time.  

Given the current time is 15:05:38.

| Symbol | Description            | Example   | Result   |
|--------|------------------------|-----------|----------|
| h      | add/subtract # hours   | now(+1h)  | 16:05:38 |
| m      | add/subtract # minutes | now(+23m) | 16:28:38 |
| s      | add/subtract # secons  | now(+12s) | 16:05:50 |

A more complex, but valid use case would be: `now(+2h-50s)` wich translates to now plus 2 hours, minus 50 seconds.
Even `now(+3h-40m+12s)` would return a valid time.

## Example

Remember to wrap functions into the `${}` format!

| Action name | Object    | Data          | Result                                                      |
|-------------|-----------|---------------|-------------------------------------------------------------|
| type        | id=lst-ib | ${today()}    | Todays date, formatted as configured                        |
| type        | id=lst-ib | ${now()}      | Current time, formatted as configured                       |
| verifyText  | id=lst-ib | ${today(+2d)} | verifies wheter the text is equal to the day after tomorrow |


## Configuration

The format of the date and time can be adjusted by editing the `config.json` file, located in `C:\Program Files\JOSF\src\main\conf\config.json`. In this file, (if not already present) two configuration properties can be added; `FormatDate` & `FormatTime` like so:

``` json
  "FormatTime" : "kk:mm:ss",
  "FormatDate" : "dd-MM-yyyy"
```

### Formatting date & time

Formatting date and time uses a specific pattern. In the example above, the FormatDate `dd-MM-yyyy` formats a date as 25-10-2018. For a month-day-year format, we can use `MM-dd-yyyy`. If we'd want to use slashes (`/`), instead of dashes(`-`), we can set a format as `dd/MM/yyyy`.

For time, the same rules apply. The above example gives us a time (24 hours) format of hour:minute:second, or 22:01:20.

All supported formatting patterns can be reviewed in this table.

| Symbol | Meaning                    | Presentation | Examples                                       |
|--------|----------------------------|--------------|------------------------------------------------|
| G      | era                        | text         | AD; Anno Domini; A                             |
| u      | year                       | year         | 2004; 04                                       |
| y      | year-of-era                | year         | 2004; 04                                       |
| D      | day-of-year                | number       | 189                                            |
| M/L    | month-of-year              | number/text  | 7; 07; Jul; July; J                            |
| d      | day-of-month               | number       | 10                                             |
|        |                            |              |                                                |
| Q/q    | quarter-of-year            | number/text  | 3; 03; Q3; 3rd quarter                         |
| Y      | week-based-year            | year         | 1996; 96                                       |
| w      | week-of-week-based-year    | number       | 27                                             |
| W      | week-of-month              | number       | 4                                              |
| E      | day-of-week                | text         | Tue; Tuesday; T                                |
| e/c    | localized day-of-week      | number/text  | 2; 02; Tue; Tuesday; T                         |
| F      | week-of-month              | number       | 3                                              |
|        |                            |              |                                                |
| a      | am-pm-of-day               | text         | PM                                             |
| h      | clock-hour-of-am-pm (1-12) | number       | 12                                             |
| K      | hour-of-am-pm (0-11)       | number       | 0                                              |
| k      | clock-hour-of-am-pm (1-24) | number       | 0                                              |
|        |                            |              |                                                |
| H      | hour-of-day (0-23)         | number       | 0                                              |
| m      | minute-of-hour             | number       | 30                                             |
| s      | second-of-minute           | number       | 55                                             |
| S      | fraction-of-second         | fraction     | 978                                            |
| A      | milli-of-day               | number       | 1234                                           |
| n      | nano-of-second             | number       | 987654321                                      |
| N      | nano-of-day                | number       | 1234000000                                     |
|        |                            |              |                                                |
| V      | time-zone ID               | zone-id      | America/Los_Angeles; Z; -08:30                 |
| z      | time-zone name             | zone-name    | Pacific Standard Time; PST                     |
| O      | localized zone-offset      | offset-O     | GMT+8; GMT+08:00; UTC-08:00;                   |
| X      | zone-offset 'Z' for zero   | offset-X     | Z; -08; -0830; -08:30; -083015; -08:30:15;     |
| x      | zone-offset                | offset-x     | +0000; -08; -0830; -08:30; -083015; -08:30:15; |
| Z      | zone-offset                | offset-Z     | +0000; -0800; -08:00;                          |
|        |                            |              |                                                |
| p      | pad next                   | pad modifier | 1                                              |
|        |                            |              |                                                |
| '      | escape for text            | delimiter    |                                                |
| ''     | single quote               | literal      | '                                              |
| [      | optional section start     |              |                                                |
| ]      | optional section end       |              |                                                |
| #      | reserved for future use    |              |                                                |
| {      | reserved for future use    |              |                                                |
| }      | reserved for future use    |              |                                                |

For more information about formatting, please visit: [https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html](https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html)