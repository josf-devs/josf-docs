# Details
| **since**       | *2.6*            |
|-----------------|------------------|
| **action-pack** | *API*            |
| **author**      | *JOSF-core team* |
| **status**      | *active*         |

Verifies if an API requests response status code equals, is less (or equal), is greater (or equal) based on the operator, to the expected given code. 

Here is a list of status codes on [Wikipedia](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)

You can use the following operators '=, >, <, =>, =< or !='. Omit the operator to verify the exact match (equals the '=' operator)

## Example

Given an API response is present in the context with a status of 201 (Created);

| Action name        | Object | Data  | Result                                       |
|--------------------|--------|-------|----------------------------------------------|
| Verify status code |        | > 200 | Results to PASS (status is bigger than 200)  |
| Verify status code |        | < 300 | Results to PASS (status is smaller than 300) |
| Verify status code |        | < 300 | Results to PASS (status is smaller than 300) |
| Verify status code |        | < 200 | Results to FAIL (status is bigger than 200)  |

## Related actions

none
