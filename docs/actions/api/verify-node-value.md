# Details
| **since**       | *2.6*            |
|-----------------|------------------|
| **action-pack** | *API*            |
| **author**      | *JOSF-core team* |
| **status**      | *active*         |

Verifies if a node value equals, is less (or equal), is greater (or equal) in a response, based on the locator and given operator.

You can use the following operators '=, >, <, =>, =< or !='. Omit the operator to verify the exact match (equals the '=' operator)

**Note** that an XML response requires an xPath and a JSON response requires JsonPath.


## Example

Given this API response is present in the context;
****
``` xml
<manifest>
  <person>
    <firstName>Jack</firstName>
    <lastName>Dawson</lastName>
    <age>20</age>
  </person>
  <person>
    <firstName>Rose</firstName>
    <lastName>DeWittt Bukater</lastName>
    <age>27</age>
  </person>
</manifest>
```

| Action name       | Object   | Data | Result                                        |
|-------------------|----------|------|-----------------------------------------------|
| Verify node value | //age[1] | < 30 | Results to PASS (age of Rose is less than 30) |
| Verify node value | //age[0] | 20   | Results to PASS (age of Jack is 20)           |

## Related actions

none