# Details
| **since**       | *2.6*            |
|-----------------|------------------|
| **action-pack** | *API*            |
| **author**      | *JOSF-core team* |
| **status**      | *active*         |

Verifies the expected number of nodes present in a response, based on the locator.

**Note** that an XML response requires an xPath and a JSON response requires JsonPath.

You can use the following operators '=, >, <, =>, =< or !='. Omit the operator to verify the exact match (equals the '=' operator)

## Example

Given this API response is present in the context;
****
``` xml
<manifest>
  <person>
    <firstName>Jack</firstName>
    <lastName>Dawson</lastName>
    <age>20</age>
  </person>
  <person>
    <firstName>Rose</firstName>
    <lastName>DeWittt Bukater</lastName>
    <age>27</age>
  </person>
</manifest>
```

| Action name       | Object      | Data | Result                                     |
|-------------------|-------------|------|--------------------------------------------|
| Verify node count | //person    | 2    | Results to PASS                            |
| Verify node count | //person    | < 10 | Results to PASS (count is smaller than 10) |
| Verify node count | //firstName | 2    | Results to PASS                            |
| Verify node count | //manifest  | 2    | Results to FAIL                            |

## Related actions

none

