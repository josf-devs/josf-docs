# Details
| **since**       | *2.6*            |
|-----------------|------------------|
| **action-pack** | *API*            |
| **author**      | *JOSF-core team* |
| **status**      | *active*         |

Verifies if an API requests response time equals, is less (or equal), is greater (or equal) based on the operator, to the expected milliseconds.

You can use the following operators '=, >, <, =>, =< or !='. Omit the operator to verify the exact match (equals the '=' operator)

## Example

Given an API response is present in the context with a response time of 200 milliseconds;

| Action name          | Object   | Data    | Result                                                   |
|----------------------|----------|---------|----------------------------------------------------------|
| Verify response time |          | > 100   | Results to PASS (response time is bigger than 100)       |
| Verify node value    | //age[0] | <= 1000 | Results to PASS (response time is less or equal to 1000) |

## Related actions

none
