# Details
| **since**       | *2.6*            |
|-----------------|------------------|
| **action-pack** | *API*            |
| **author**      | *JOSF-core team* |
| **status**      | *active*         |

Extracts a node from an API response into a buffer value

**Note** that an XML response requires an xPath and a JSON response requires JsonPath.

Note that ommitting the Object field, will set the entire response (in string) to the buffer.


## Example

Given this API response is present in the context;

``` xml
<person>
  <firstName>Jack</firstName>
  <lastName>Dawson</lastName>
  <age>20</age>
</person>
```

| Action name  | Object      | Data     | Result                                     |
|--------------|-------------|----------|--------------------------------------------|
| Extract Node | //firstName | name     | Sets the buffer `name` with value Jack     |
| Extract Node |             | response | Sets the buffer `response` with entire XML |

## Related actions

none

