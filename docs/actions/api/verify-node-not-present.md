# Details
| **since**       | *2.6*            |
|-----------------|------------------|
| **action-pack** | *API*            |
| **author**      | *JOSF-core team* |
| **status**      | *active*         |

Verifies if a node is **not** present in a response, based on the locator.

**Note** that an XML response requires an xPath and a JSON response requires JsonPath.


## Example

Given this API response is present in the context;
****
``` xml
<manifest>
  <person>
    <firstName>Jack</firstName>
    <lastName>Dawson</lastName>
    <age>20</age>
  </person>
  <person>
    <firstName>Rose</firstName>
    <lastName>DeWittt Bukater</lastName>
    <age>27</age>
  </person>
</manifest>
```

| Action name             | Object                        | Data | Result          |
|-------------------------|-------------------------------|------|-----------------|
| Verify node not present | //person                      |      | Results to FAIL |
| Verify node not present | //firstName[text()='Caledon'] |      | Results to PASS |

## Related actions

- [verify node present](verify-node-present.md)


