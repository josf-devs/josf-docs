# Details
| **since**       | *2.6*            |
|-----------------|------------------|
| **action-pack** | *API*            |
| **author**      | *JOSF-core team* |
| **status**      | *active*         |

Verifies if a node is present in a response, based on the locator.

**Note** that an XML response requires an xPath and a JSON response requires JsonPath.


## Example

Given this API response is present in the context;
****
``` xml
<manifest>
  <person>
    <firstName>Jack</firstName>
    <lastName>Dawson</lastName>
    <age>20</age>
  </person>
  <person>
    <firstName>Rose</firstName>
    <lastName>DeWittt Bukater</lastName>
    <age>27</age>
  </person>
</manifest>
```

| Action name         | Object                        | Data | Result          |
|---------------------|-------------------------------|------|-----------------|
| Verify node present | //person                      |      | Results to PASS |
| Verify node present   | //firstName[text()='Caledon'] |      | Results to FAIL |

## Related actions

- [verify node not present](verify-node-not-present.md)


