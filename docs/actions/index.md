# Dance moves

JOSF consists of a lot of dance moves and it's a good idea to review the use of these cool moves! These dance moves are called test actions, and they have these parameters:

- An action name;
- An object on which the action should take place; and,
- Some data that could be used.

In this section, we will handle all implemented actions that are packaged within JOSF and it's action packs. Each action is a manual on its own and comes with a couple of examples.

## Action packs

Because the world is in constant change, JOSF is open to learning new stuff. That's why our engineers have built JOSF so, that you can add your own actions. Besides your own actions, we've hit you up with a Selenese action pack to get you started.

### Selenese action pack

The default pack, is the Selenese action pack. [Selenese](http://www.seleniumhq.org/docs/02_selenium_ide.jsp#selenium-commands-selenese) is the term for the actions that are defined by Selenium. 

### API action pack

The API action pack consists of actions to verify, control and test your API's!
